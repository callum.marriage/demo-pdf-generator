FROM gcr.io/distroless/java:11
EXPOSE 9502
COPY ./target/ms-pdf-generator*.jar /ms-pdf-generator.jar
COPY ./jsonValidator.json /jsonValidator.json
COPY ./config.yml /config.yml

ENTRYPOINT [ "java", "-jar", "/ms-pdf-generator.jar", "server", "/config.yml" ]
