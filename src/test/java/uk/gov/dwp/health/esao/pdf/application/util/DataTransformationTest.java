package uk.gov.dwp.health.esao.pdf.application.util;

import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DataTransformationTest {

//  private DataTransformation dataTransformation;
//  private Pensions pensions1, pensions2;
//  private Insurances insurances1;
//  private Employments employments1;
//  private VoluntaryWork voluntaryWork1;
//  private RequestJson requestJson;
//  private List<Insurances> insurancesList = new ArrayList<>();
//  private List<Pensions> pensionsList = new ArrayList<>();
//  private List<Employments> employmentsList = new ArrayList<>();
//  private List<VoluntaryWork> voluntaryWorksList = new ArrayList<>();
//  private List<DeductionDetails> deductionDetailsList = new ArrayList<>();
//  private String templateHtml;
//
//  private MedicalCentre medicalCentre;
//  private MandatoryAddress mandatoryAddress;
//  private NonMandatoryAddress nonMandatoryAddress;
//  private List<Conditions> conditionsList = new ArrayList<>();
//  private Applicant applicant;
//  private List<String> tags = new ArrayList<>();
//  private DataCapture dataCapture;
//  private ContactOptions contactOptions;
//  private List<ContactOptions> contactOptionsList;
//  private StringSubstitutor substitutor;
//  private String resolvedHtml;
//  private Map<String, String> valuesMap;
//  private ArrayList<String> employmentStatus;
//  private int normalHashMapSize = 67;
//
//  @Mock
//  private InputStream inputStream;
//
//
//  @Before
//  public void setup() throws IOException {
//
//    contactOptionsList = new ArrayList<>();
//    List<String> lines = new ArrayList<>();
//    lines.add("abc");
//    lines.add("xyz");
//
//    Conditions conditions;
//
//    DeductionDetails deductionDetails;
//
//    employmentStatus = new ArrayList<>();
//    employmentStatus.add("employee");
//    employmentStatus.add("selfEmployed");
//
//    deductionDetails = new DeductionDetails("1", "stuff");
//    deductionDetailsList.add(deductionDetails);
//
//    dataTransformation = new DataTransformation();
//    mandatoryAddress = new MandatoryAddress();
//    mandatoryAddress.setLines(lines);
//    mandatoryAddress.setPremises("premises");
//    mandatoryAddress.setPostCode("ABC 123");
//
//    nonMandatoryAddress = new NonMandatoryAddress();
//    nonMandatoryAddress.setLines(null);
//    nonMandatoryAddress.setPremises(null);
//    nonMandatoryAddress.setPostCode(null);
//
//    pensions1 = new Pensions("provider", "00-12-123", "01234", nonMandatoryAddress, "2018-03-05",
//        "no", null, "every4Weeks", "yes", "1200", null);
//
//    pensions2 = new Pensions("provider", "00-12-123", "01234", nonMandatoryAddress, "2018-03-05",
//        "yes", "1234", "every4Weeks", "yes", "1200", deductionDetailsList);
//
//    insurances1 = new Insurances("Ins-provider", "00-11-22", "0123456", nonMandatoryAddress,
//        "18", "every2Weeks", "100", "2018-10-10");
//
//    employments1 = new Employments("SE", "john", "075656565", mandatoryAddress, employmentStatus, "yes", "2019-01-01",
//        "yes", "7", "daily", "100", "yes", "", "detail");
//
//    voluntaryWork1 = new VoluntaryWork("ABC-org", mandatoryAddress, "IT", "yes", "5");
//
//    contactOptions = new ContactOptions("contactId", "telMobile", "07666", true);
//
//    medicalCentre = new MedicalCentre("NHS NCL", "067787787", mandatoryAddress, "Dr ABC");
//
//    List<ContactOptions> contactOptionsList = new ArrayList<>();
//    contactOptionsList.add(contactOptions);
//
//    conditions = new Conditions("fever", "2019-01-01");
//
//    conditionsList.add(conditions);
//
//
//    dataCapture = new DataCapture("en", conditionsList, medicalCentre, "yes", "yes", "yes", "yes", "maternity", "AB101010C", "John", "Llyods", "01-01-01",
//        null, "12312312", "2019-01-01", "yes", "yes", "2019-02-02", "yes", "2019-02-02", "yes", "2019-02-02", "yes", "termprog", "yes", "hosp_name",
//        "hosp-ward", "2019-03-03", "yes", voluntaryWorksList, "yes", employmentsList, mandatoryAddress, "yes", "2019-02-10", "yes", "2019-02-02", "yes", null,
//        pensionsList, "yes", insurancesList, "yes", "no", "yes", "high-risk", "high risk description", "yes", "2019-02-02", "yes", null, null, "no", null, null);
//
//
//    tags.add("_67678688");
//    applicant = new Applicant(mandatoryAddress, "FName", "SName", "01-01-1981", contactOptionsList);
//    requestJson = new RequestJson("msgId", applicant, "ref", "2019-04-11T10:02:07.098Z", dataCapture, "declaration", tags, "08988");
//
//    templateHtml = IOUtils.toString(getClass().getResourceAsStream(HTMLTemplateFiles.ESAO_TEMPLATE));
//  }
//
//  @Test
//  public void testTransformPensionDetail() throws ParseException, IOException {
//
//
//    pensionsList.add(pensions1);
//
//    String expectedResult =   "<br/><br/>\n" +
//                                  "<table>\n" +
//                                  "    <h2>Pension (1 of 1)</h2>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Pension provider</td>\n" +
//                                  "        <td class=\"table-td-value\">provider</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Date started</td>\n" +
//                                  "        <td class=\"table-td-value\">05/03/18</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Address</td>\n" +
//                                  "        <td class=\"table-td-value\"></td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Postcode</td>\n" +
//                                  "        <td class=\"table-td-value\"></td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Telephone number</td>\n" +
//                                  "        <td class=\"table-td-value\">01234</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Reference number</td>\n" +
//                                  "        <td class=\"table-td-value\">00-12-123</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Deductions from pension or annuity</td>\n" +
//                                  "        <td class=\"table-td-value\">No</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr style=\"display:none;\">\n" +
//                                  "        <td class=\"table-td-caption\">Payment frequency of pension or annuity</td>\n" +
//                                  "        <td class=\"table-td-value\">every 4 weeks</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr style=\"display:table-row;\">\n" +
//                                  "        <td class=\"table-td-caption\">Payments from pension or annuity</td>\n" +
//                                  "        <td class=\"table-td-value\">&#163;${paymentsFromPension} every 4 weeks</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr style=\"display:none;\">\n" +
//                                  "        <td class=\"table-td-caption\">Payments from pension or annuity</td>\n" +
//                                  "        <td class=\"table-td-value\">&#163;${paymentsFromPensionBeforeDeduction} before deductions<br/>&#163;${paymentsFromPensionAfterDeduction} after deductions</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr style=\"display:none;\">\n" +
//                                  "        <td class=\"table-td-caption\">Deduction details</td>\n" +
//                                  "        <td class=\"table-td-value\">${pensionDeductionDetail}</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Inherited pension or annuity</td>\n" +
//                                  "        <td class=\"table-td-value\">Yes</td>\n" +
//                                  "    </tr>\n" +
//                                  "</table>\n" +
//                                  "<br/>";
//
//
//    String transformedPension = dataTransformation.transformPensionDetail(pensionsList);
//
//    assertEquals(transformedPension.trim().replaceAll("\r", ""), expectedResult.trim());
//  }
//
//  @Test
//  public void testTransformPensionDetailWithDeductions() throws ParseException, IOException {
//
//
//    pensionsList.add(pensions2);
//
//    String expectedResult =   "<br/><br/>\n" +
//                                  "<table>\n" +
//                                  "    <h2>Pension (1 of 1)</h2>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Pension provider</td>\n" +
//                                  "        <td class=\"table-td-value\">provider</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Date started</td>\n" +
//                                  "        <td class=\"table-td-value\">05/03/18</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Address</td>\n" +
//                                  "        <td class=\"table-td-value\"></td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Postcode</td>\n" +
//                                  "        <td class=\"table-td-value\"></td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Telephone number</td>\n" +
//                                  "        <td class=\"table-td-value\">01234</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Reference number</td>\n" +
//                                  "        <td class=\"table-td-value\">00-12-123</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Deductions from pension or annuity</td>\n" +
//                                  "        <td class=\"table-td-value\">Yes</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr style=\"display:table-row;\">\n" +
//                                  "        <td class=\"table-td-caption\">Payment frequency of pension or annuity</td>\n" +
//                                  "        <td class=\"table-td-value\">Every 4 weeks</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr style=\"display:none;\">\n" +
//                                  "        <td class=\"table-td-caption\">Payments from pension or annuity</td>\n" +
//                                  "        <td class=\"table-td-value\">&#163;1234 Every 4 weeks</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr style=\"display:table-row;\">\n" +
//                                  "        <td class=\"table-td-caption\">Payments from pension or annuity</td>\n" +
//                                  "        <td class=\"table-td-value\">&#163;1234 before deductions<br/>&#163;1200 after deductions</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr style=\"display:table-row;\">\n" +
//                                  "        <td class=\"table-td-caption\">Deduction details</td>\n" +
//                                  "        <td class=\"table-td-value\">&#163;1 stuff<br/></td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Inherited pension or annuity</td>\n" +
//                                  "        <td class=\"table-td-value\">Yes</td>\n" +
//                                  "    </tr>\n" +
//                                  "</table>\n" +
//                                  "<br/>";
//
//
//    String transformedPension = dataTransformation.transformPensionDetail(pensionsList);
//
//    assertEquals(transformedPension.trim().replaceAll("\r", ""), expectedResult.trim());
//  }
//
//  @Test(expected = IOException.class)
//  public void testTransformPensionDetailThrowException() throws ParseException, IOException {
//    List<Pensions> pensionsList = new ArrayList<>();
//    pensionsList.add(pensions1);
//    when(IOUtils.toString(inputStream)).thenThrow(new IOException());
//    dataTransformation.transformPensionDetail(pensionsList);
//  }
//
//  @Test
//  public void testTransformInsuranceDetail() throws IOException, ParseException {
//
//    insurancesList.add(insurances1);
//    String expectedResult =   "<br/><br/>\n" +
//                                  "<table>\n" +
//                                  "    <h2>Permanent health insurance (1 of 1)</h2>\n" +
//                                  "    <table>\n" +
//                                  "        <tr>\n" +
//                                  "            <td class=\"table-td-caption\">Insurance company</td>\n" +
//                                  "            <td class=\"table-td-value\">Ins-provider</td>\n" +
//                                  "        </tr>\n" +
//                                  "        <tr>\n" +
//                                  "            <td class=\"table-td-caption\">Address</td>\n" +
//                                  "            <td class=\"table-td-value\"></td>\n" +
//                                  "        </tr>\n" +
//                                  "        <tr>\n" +
//                                  "            <td class=\"table-td-caption\">Postcode</td>\n" +
//                                  "            <td class=\"table-td-value\"></td>\n" +
//                                  "        </tr>\n" +
//                                  "        <tr>\n" +
//                                  "            <td class=\"table-td-caption\">Telephone number</td>\n" +
//                                  "            <td class=\"table-td-value\">0123456</td>\n" +
//                                  "        </tr>\n" +
//                                  "        <tr>\n" +
//                                  "            <td class=\"table-td-caption\">Reference number</td>\n" +
//                                  "            <td class=\"table-td-value\">00-11-22</td>\n" +
//                                  "        </tr>\n" +
//                                  "        <tr>\n" +
//                                  "            <td class=\"table-td-caption\">Premiums paid</td>\n" +
//                                  "            <td class=\"table-td-value\">I'm not sure</td>\n" +
//                                  "        </tr>\n" +
//                                  "        <tr>\n" +
//                                  "            <td class=\"table-td-caption\">Still working for the employer that setup the policy</td>\n" +
//                                  "            <td class=\"table-td-value\">No</td>\n" +
//                                  "        </tr>\n" +
//                                  "        <tr style=\"display:table-row;\">\n" +
//                                  "            <td class=\"table-td-caption\">Employment ended on</td>\n" +
//                                  "            <td class=\"table-td-value\">10/10/18</td>\n" +
//                                  "        </tr>\n" +
//                                  "        <tr>\n" +
//                                  "            <td class=\"table-td-caption\">Payments received from health insurance</td>\n" +
//                                  "            <td class=\"table-td-value\">&#163;18 every 2 weeks</td>\n" +
//                                  "        </tr>\n" +
//                                  "    </table>\n" +
//                                  "</table>\n" +
//                                  "<br/>";
//
//    String transformedInsurance = dataTransformation.transformInsuranceDetail(insurancesList);
//
//    assertEquals(transformedInsurance.trim().replaceAll("\r", ""), expectedResult.trim());
//  }
//
//  @Test
//  public void testTransformInsuranceDetailWithNoEmpEndDate() throws IOException, ParseException {
//
//    insurances1.setEmploymentEndDate("-0-0");
//    insurancesList.add(insurances1);
//    String expectedResult =   "<br/><br/>\n" +
//                                  "<table>\n" +
//                                  "    <h2>Permanent health insurance (1 of 1)</h2>\n" +
//                                  "    <table>\n" +
//                                  "        <tr>\n" +
//                                  "            <td class=\"table-td-caption\">Insurance company</td>\n" +
//                                  "            <td class=\"table-td-value\">Ins-provider</td>\n" +
//                                  "        </tr>\n" +
//                                  "        <tr>\n" +
//                                  "            <td class=\"table-td-caption\">Address</td>\n" +
//                                  "            <td class=\"table-td-value\"></td>\n" +
//                                  "        </tr>\n" +
//                                  "        <tr>\n" +
//                                  "            <td class=\"table-td-caption\">Postcode</td>\n" +
//                                  "            <td class=\"table-td-value\"></td>\n" +
//                                  "        </tr>\n" +
//                                  "        <tr>\n" +
//                                  "            <td class=\"table-td-caption\">Telephone number</td>\n" +
//                                  "            <td class=\"table-td-value\">0123456</td>\n" +
//                                  "        </tr>\n" +
//                                  "        <tr>\n" +
//                                  "            <td class=\"table-td-caption\">Reference number</td>\n" +
//                                  "            <td class=\"table-td-value\">00-11-22</td>\n" +
//                                  "        </tr>\n" +
//                                  "        <tr>\n" +
//                                  "            <td class=\"table-td-caption\">Premiums paid</td>\n" +
//                                  "            <td class=\"table-td-value\">I'm not sure</td>\n" +
//                                  "        </tr>\n" +
//                                  "        <tr>\n" +
//                                  "            <td class=\"table-td-caption\">Still working for the employer that setup the policy</td>\n" +
//                                  "            <td class=\"table-td-value\">No</td>\n" +
//                                  "        </tr>\n" +
//                                  "        <tr style=\"display:table-row;\">\n" +
//                                  "            <td class=\"table-td-caption\">Employment ended on</td>\n" +
//                                  "            <td class=\"table-td-value\">Not provided</td>\n" +
//                                  "        </tr>\n" +
//                                  "        <tr>\n" +
//                                  "            <td class=\"table-td-caption\">Payments received from health insurance</td>\n" +
//                                  "            <td class=\"table-td-value\">&#163;18 every 2 weeks</td>\n" +
//                                  "        </tr>\n" +
//                                  "    </table>\n" +
//                                  "</table>\n" +
//                                  "<br/>";
//
//    String transformedInsurance = dataTransformation.transformInsuranceDetail(insurancesList);
//
//    assertEquals(transformedInsurance.trim().replaceAll("\r", ""), expectedResult.trim());
//  }
//
//  @Test
//  public void testTransformInsuranceDetailWithStillWorkingForSameEmp() throws IOException, ParseException {
//
//    insurances1.setEmploymentEndDate(null);
//    insurancesList.add(insurances1);
//    String expectedResult =   "<br/><br/>\n" +
//                                  "<table>\n" +
//                                  "    <h2>Permanent health insurance (1 of 1)</h2>\n" +
//                                  "    <table>\n" +
//                                  "        <tr>\n" +
//                                  "            <td class=\"table-td-caption\">Insurance company</td>\n" +
//                                  "            <td class=\"table-td-value\">Ins-provider</td>\n" +
//                                  "        </tr>\n" +
//                                  "        <tr>\n" +
//                                  "            <td class=\"table-td-caption\">Address</td>\n" +
//                                  "            <td class=\"table-td-value\"></td>\n" +
//                                  "        </tr>\n" +
//                                  "        <tr>\n" +
//                                  "            <td class=\"table-td-caption\">Postcode</td>\n" +
//                                  "            <td class=\"table-td-value\"></td>\n" +
//                                  "        </tr>\n" +
//                                  "        <tr>\n" +
//                                  "            <td class=\"table-td-caption\">Telephone number</td>\n" +
//                                  "            <td class=\"table-td-value\">0123456</td>\n" +
//                                  "        </tr>\n" +
//                                  "        <tr>\n" +
//                                  "            <td class=\"table-td-caption\">Reference number</td>\n" +
//                                  "            <td class=\"table-td-value\">00-11-22</td>\n" +
//                                  "        </tr>\n" +
//                                  "        <tr>\n" +
//                                  "            <td class=\"table-td-caption\">Premiums paid</td>\n" +
//                                  "            <td class=\"table-td-value\">I'm not sure</td>\n" +
//                                  "        </tr>\n" +
//                                  "        <tr>\n" +
//                                  "            <td class=\"table-td-caption\">Still working for the employer that setup the policy</td>\n" +
//                                  "            <td class=\"table-td-value\">Yes</td>\n" +
//                                  "        </tr>\n" +
//                                  "        <tr style=\"display:none;\">\n" +
//                                  "            <td class=\"table-td-caption\">Employment ended on</td>\n" +
//                                  "            <td class=\"table-td-value\">${employmentEndedOn}</td>\n" +
//                                  "        </tr>\n" +
//                                  "        <tr>\n" +
//                                  "            <td class=\"table-td-caption\">Payments received from health insurance</td>\n" +
//                                  "            <td class=\"table-td-value\">&#163;18 every 2 weeks</td>\n" +
//                                  "        </tr>\n" +
//                                  "    </table>\n" +
//                                  "</table>\n" +
//                                  "<br/>";
//
//    String transformedInsurance = dataTransformation.transformInsuranceDetail(insurancesList);
//
//    assertEquals(transformedInsurance.trim().replaceAll("\r", ""), expectedResult.trim());
//  }
//
//  @Test
//  public void testTransformEmploymentDetailWhenOffSickYes() throws IOException, ParseException {
//
//    employmentsList.add(employments1);
//
//    String expectedResult =   "<br/><br/>\n" +
//                                  "<table>\n" +
//                                  "    <h2>Paid work (1 of 1)</h2>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Employment status</td>\n" +
//                                  "        <td class=\"table-td-value\">Employee<br/>Self-employed</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Employer</td>\n" +
//                                  "        <td class=\"table-td-value\">john</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Address</td>\n" +
//                                  "        <td class=\"table-td-value\">abc<br/>xyz<br/>premises<br/></td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Postcode</td>\n" +
//                                  "        <td class=\"table-td-value\">ABC 123</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Telephone number</td>\n" +
//                                  "        <td class=\"table-td-value\">075656565</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Employed as</td>\n" +
//                                  "        <td class=\"table-td-value\">SE</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Off sick from this work</td>\n" +
//                                  "        <td class=\"table-td-value\">Yes</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr style=\"display:table-row;\">\n" +
//                                  "        <td class=\"table-td-caption\">Last worked</td>\n" +
//                                  "        <td class=\"table-td-value\">01/01/19</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr style=\"display:table-row;\">\n" +
//                                  "        <td class=\"table-td-caption\">Pay</td>\n" +
//                                  "        <td class=\"table-td-value\">&#163;100 a day</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr style=\"display:table-row;\">\n" +
//                                  "        <td class=\"table-td-caption\">Hours worked a week</td>\n" +
//                                  "        <td class=\"table-td-value\">7</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr style=\"display:table-row;\">\n" +
//                                  "        <td class=\"table-td-caption\">Help from a professional support worker</td>\n" +
//                                  "        <td class=\"table-td-value\">Yes</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr style=\"display:table-row;\">\n" +
//                                  "        <td class=\"table-td-caption\">Expenses</td>\n" +
//                                  "        <td class=\"table-td-value\">detail</td>\n" +
//                                  "    </tr>\n" +
//                                  "</table>\n" +
//                                  "<br/>";
//
//
//    String transformedEmployments = dataTransformation.transformEmployments(employmentsList);
//    assertEquals(transformedEmployments.trim().replaceAll("\r", ""), expectedResult.trim());
//  }
//
//  @Test
//  public void testTransformEmploymentDetailWhenOffSickNo() throws IOException, ParseException {
//    employments1.setOffSick("no");
//    employmentsList.add(employments1);
//
//    String expectedResult =   "<br/><br/>\n" +
//                                  "<table>\n" +
//                                  "    <h2>Paid work (1 of 1)</h2>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Employment status</td>\n" +
//                                  "        <td class=\"table-td-value\">Employee<br/>Self-employed</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Employer</td>\n" +
//                                  "        <td class=\"table-td-value\">john</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Address</td>\n" +
//                                  "        <td class=\"table-td-value\">abc<br/>xyz<br/>premises<br/></td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Postcode</td>\n" +
//                                  "        <td class=\"table-td-value\">ABC 123</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Telephone number</td>\n" +
//                                  "        <td class=\"table-td-value\">075656565</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Employed as</td>\n" +
//                                  "        <td class=\"table-td-value\">SE</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Off sick from this work</td>\n" +
//                                  "        <td class=\"table-td-value\">No</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr style=\"display:none;\">\n" +
//                                  "        <td class=\"table-td-caption\">Last worked</td>\n" +
//                                  "        <td class=\"table-td-value\">${lastWorked}</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr style=\"display:table-row;\">\n" +
//                                  "        <td class=\"table-td-caption\">Pay</td>\n" +
//                                  "        <td class=\"table-td-value\">&#163;100 a day</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr style=\"display:table-row;\">\n" +
//                                  "        <td class=\"table-td-caption\">Hours worked a week</td>\n" +
//                                  "        <td class=\"table-td-value\">7</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr style=\"display:table-row;\">\n" +
//                                  "        <td class=\"table-td-caption\">Help from a professional support worker</td>\n" +
//                                  "        <td class=\"table-td-value\">Yes</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr style=\"display:table-row;\">\n" +
//                                  "        <td class=\"table-td-caption\">Expenses</td>\n" +
//                                  "        <td class=\"table-td-value\">detail</td>\n" +
//                                  "    </tr>\n" +
//                                  "</table>\n" +
//                                  "<br/>";
//
//
//    String transformedEmployments = dataTransformation.transformEmployments(employmentsList);
//    assertEquals(transformedEmployments.trim().replaceAll("\r", ""), expectedResult.trim());
//  }
//
//  @Test
//  public void testTransformVoluntaryWork() throws IOException {
//
//    voluntaryWorksList.add(voluntaryWork1);
//    String expectedResult =   "<br/><br/>\n" +
//                                  "<table>\n" +
//                                  "    <h2>Voluntary work (1 of 1)</h2>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Voluntary organisation</td>\n" +
//                                  "        <td class=\"table-td-value\">ABC-org</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Voluntary job address</td>\n" +
//                                  "        <td class=\"table-td-value\">abc<br/>xyz<br/>premises<br/></td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Voluntary job postcode</td>\n" +
//                                  "        <td class=\"table-td-value\">ABC 123</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Type of voluntary work</td>\n" +
//                                  "        <td class=\"table-td-value\">IT</td>\n" +
//                                  "    </tr>\n" +
//                                  "    <tr>\n" +
//                                  "        <td class=\"table-td-caption\">Hours they volunteer each week</td>\n" +
//                                  "        <td class=\"table-td-value\">5</td>\n" +
//                                  "    </tr>\n" +
//                                  "</table>\n" +
//                                  "<br/>";
//
//
//    String transformedVoluntaryWork = dataTransformation.transformVoluntaryWork(voluntaryWorksList);
//    assertEquals(transformedVoluntaryWork.trim().replaceAll("\r", ""), expectedResult.trim());
//  }
//
//
//  private String transformRequest(RequestJson requestJson) throws ParseException, IOException {
//    valuesMap = dataTransformation.transformData(requestJson);
//    substitutor = new StringSubstitutor(valuesMap);
//    return substitutor.replace(templateHtml);
//  }
//
//  @Test
//  public void testTransformJson1WithNoMobileNo() throws ParseException, IOException {
//
//    contactOptions.setData(null);
//    contactOptionsList.add(contactOptions);
//    applicant.setContactOptionsList(contactOptionsList);
//    applicant.setContactOptionsList(contactOptionsList);
//    requestJson.setApplicant(applicant);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should not display mobile number", resolvedHtml.replaceAll("\r", ""),
//        CoreMatchers.containsString("    <tr>\n" +
//                                        "        <td class=\"table-td-caption\">Mobile phone number</td>\n" +
//                                        "        <td class=\"table-td-value\">No</td>\n" +
//                                        "    </tr>"));
//
//  }
//
//  @Test
//  public void testTransformJson3WithNoLifeExpectancy() throws ParseException, IOException {
//
//    dataCapture.setSevereCondition("no");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should not display life expectancy", resolvedHtml.replaceAll("\r", ""),
//        CoreMatchers.containsString("More than 6 months"));
//
//  }
//
//  @Test
//  public void testTransformJson4WithLifeExpectancy() throws ParseException, IOException {
//
//    dataCapture.setSevereCondition("yes");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display life expectancy", resolvedHtml.replaceAll("\r", ""),
//        CoreMatchers.containsString("Less than 6 months"));
//
//  }
//
//  @Test
//  public void testTransformJson5BankDetail() throws ParseException, IOException {
//
//    dataCapture.setBankAccountName("John");
//    dataCapture.setBankRollNumber("abc-123");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display bank detail", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Account name</td>\n<td class=\"table-td-value\">John</td>"));
//    assertThat("should display bank detail", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Building society roll number</td>\n<td class=\"table-td-value\">abc-123</td>"));
//
//  }
//
//  @Test
//  public void testTransformJson6WithPregnancy() throws ParseException, IOException {
//
//    dataCapture.setPregnant("yes");
//    dataCapture.setDueDate("2019-02-02");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display pregnancy detail", resolvedHtml.replaceAll("\r", ""),
//        CoreMatchers.containsString("Baby due on"));
//    assertThat("should display pregnancy detail", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Pregnant</td>\n<td class=\"table-td-value\">Yes<br/>"));
//
//
//  }
//
//  @Test
//  public void testTransformJson7WithNoPregnancy() throws ParseException, IOException {
//
//    dataCapture.setPregnant("no");
//    dataCapture.setDueDate(null);
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should not display pregnancy detail", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("Pregnant</td>\n" +
//                                        "<td class=\"table-td-value\">No"));
//
//  }
//
//  @Test
//  public void testTransformJson8WithMobile() throws ParseException, IOException {
//
//    contactOptions = new ContactOptions("contactId", "telmobile", "07666", true);
//    contactOptionsList.add(contactOptions);
//    applicant.setContactOptionsList(contactOptionsList);
//    applicant.setContactOptionsList(contactOptionsList);
//    requestJson.setApplicant(applicant);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display mobile detail", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Mobile phone number</td>\n" +
//                                        "<td class=\"table-td-value\">07666</td>\n</tr>\n<tr style=\"display:none;\">\n<td class=\"table-td-caption\">Phone number</td>\n" +
//                                        "<td class=\"table-td-value\">${otherNumber}</td>\n</tr>"));
//
//  }
//
//  @Test
//  public void testTransformJson9WithOtherPhone() throws ParseException, IOException {
//
//    contactOptions = new ContactOptions("contactId", "tel", "076667657578", true);
//    contactOptionsList.add(contactOptions);
//    applicant.setContactOptionsList(contactOptionsList);
//    applicant.setContactOptionsList(contactOptionsList);
//    requestJson.setApplicant(applicant);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display other phone detail", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Mobile phone number</td>\n" +
//                                        "<td class=\"table-td-value\">No</td>\n</tr>\n<tr style=\"display:table-row;\">\n<td class=\"table-td-caption\">Phone number</td>\n" +
//                                        "<td class=\"table-td-value\">076667657578</td>\n</tr>"));
//
//  }
//
//  @Test
//  public void testTransformJson10WithNoFitToWorkDate() throws ParseException, IOException {
//
//    dataCapture.setBackToWork("no");
//    dataCapture.setBackToWorkDate(null);
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should not display fit to work detail", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Return to work date</td>\n<td class=\"table-td-value\">No</td>"));
//
//  }
//
//  @Test
//  public void testTransformJson11WithFitToWork() throws ParseException, IOException {
//
//    dataCapture.setBackToWork("yes");
//    dataCapture.setBackToWorkDate("2019-02-02");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display fit to work detail", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Return to work date</td>\n" +
//                                        "<td class=\"table-td-value\">02/02/19"));
//
//  }
//
//
//  @Test
//  public void testTransformJson12WithHospitalDetail() throws ParseException, IOException {
//
//    dataCapture.setHospitalInpatient("yes");
//    dataCapture.setHospitalAdmissionDate("2019-03-03");
//    dataCapture.setHospitalName("hosp_name");
//    dataCapture.setHospitalWard("hosp-ward");
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display hospital detail", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Hospital details</td>\n" +
//                                        "<td class=\"table-td-value\">hosp-ward<br/>hosp_name<br/>Admitted on 03/03/19"));
//
//  }
//
//
//  @Test
//  public void testTransformJson13WithNoHospitalDetail() throws ParseException, IOException {
//
//    dataCapture.setHospitalInpatient("no");
//    dataCapture.setHospitalAdmissionDate(null);
//    dataCapture.setHospitalName(null);
//    dataCapture.setHospitalWard(null);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should not display hospital detail", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<tr>\n" +
//                                        "<td class=\"table-td-caption\">Currently in hospital</td>\n<td class=\"table-td-value\">No</td>\n</tr>"));
//
//  }
//
//
//  @Test
//  public void testTransformJson16WithNoVoluntaryWork() throws ParseException, IOException {
//
//    dataCapture.setVoluntaryWorkQuestion("no");
//    dataCapture.setVoluntaryWork(null);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should not display voluntary work details", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Doing voluntary work</td>\n" +
//                                        "<td class=\"table-td-value\">No</td>"));
//
//  }
//
//  @Test
//  public void testTransformJson17WithNoEmployments() throws ParseException, IOException {
//
//    dataCapture.setEmploymentQuestion("no");
//    dataCapture.setEmployments(null);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should not display employment details", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Currently employed</td>\n" +
//                                        "<td class=\"table-td-value\">No</td>"));
//  }
//
//
//  @Test
//  public void testTransformJson18WithNoPensions() throws ParseException, IOException {
//
//    dataCapture.setPensionQuestion("no");
//    dataCapture.setPensions(null);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should not display pension details", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("Getting money from a pension or annuity</td>\n" +
//                                        "<td class=\"table-td-value\">No</td>"));
//  }
//
//  @Test
//  public void testTransformJson19WithNoInsurances() throws ParseException, IOException {
//
//    dataCapture.setInsuranceQuestion("no");
//    dataCapture.setInsurances(null);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should not display insurance details", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("Getting money from permanent health insurance</td>\n" +
//                                        "<td class=\"table-td-value\">No</td>"));
//  }
//
//  @Test
//  public void testTransformJson20WithSSP() throws ParseException, IOException {
//
//    dataCapture.setSsp("yes");
//    dataCapture.setSspEnd("2019-02-10");
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display SSP details", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("Getting Statutory Sick Pay</td>\n" +
//                                        "<td class=\"table-td-value\">Yes</td>"));
//  }
//
//  @Test
//  public void testTransformJson21WithNoSSP() throws ParseException, IOException {
//
//    dataCapture.setSsp(null);
//    dataCapture.setSspEnd(null);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should not display SSP details", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("Getting Statutory Sick Pay</td>\n" +
//                                        "<td class=\"table-td-value\">${gettingSSP}</td>"));
//  }
//
//  @Test
//  public void testTransformJson22WithNoRecentSSP() throws ParseException, IOException {
//
//    dataCapture.setSspRecent(null);
//    dataCapture.setSspRecentEnd(null);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should not display Recent SSP details", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Received Statutory Sick Pay within 13 weeks before claim start date</td>\n" +
//                                        "<td class=\"table-td-value\">${receivedSSPInLast12Weeks}</td>"));
//  }
//
//  @Test
//  public void testTransformJson23WithRecentSSP() throws ParseException, IOException {
//
//    dataCapture.setSspRecent("yes");
//    dataCapture.setSspRecentEnd("2019-02-02");
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display Recent SSP details", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Received Statutory Sick Pay within 13 weeks before claim start date</td>\n" +
//                                        "<td class=\"table-td-value\">Yes</td>"));
//  }
//
//  @Test
//  public void testTransformJson24MedicalCentre() throws ParseException, IOException {
//
//    medicalCentre = new MedicalCentre("NHS NCL", "067787787", mandatoryAddress, "Dr ABC");
//    dataCapture.setMedicalCentre(medicalCentre);
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display Medical Centre details", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Doctor's details</td>\n" +
//                                        "<td class=\"table-td-value\">Dr ABC<br/>NHS NCL<br/>abc<br/>xyz<br/>premises<br/>ABC 123<br/>067787787</td>\n"));
//  }
//
//  @Test
//  public void testTransformJson25DoctorConsentYes() throws ParseException, IOException {
//
//    dataCapture.setDocShareWithDWP("yes");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display Doctor consent as Yes", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Doctor's consent</td>\n" +
//                                        "<td class=\"table-td-value\">Yes, the doctor can share details with DWP</td>"));
//  }
//
//  @Test
//  public void testTransformJson26DoctorConsentNo() throws ParseException, IOException {
//
//    dataCapture.setDocShareWithDWP("no");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display Doctor consent as No", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Doctor's consent</td>\n" +
//                                        "<td class=\"table-td-value\">No, the doctor cannot share details with DWP</td>"));
//  }
//
//  @Test
//  public void testTransformJson27DWPConsentYes() throws ParseException, IOException {
//
//    dataCapture.setDwpShareWithDoc("yes");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display DWP consent as Yes", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">DWP's consent</td>\n" +
//                                        "<td class=\"table-td-value\">Yes, DWP can share the outcome of an assessment with the doctor</td>\n"));
//  }
//
//  @Test
//  public void testTransformJson28DWPConsentNo() throws ParseException, IOException {
//
//    dataCapture.setDwpShareWithDoc("no");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display DWP consent as No", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">DWP's consent</td>\n" +
//                                        "<td class=\"table-td-value\">No, DWP cannot share the outcome of an assessment with the doctor</td>"));
//  }
//
//  @Test
//  public void testTransformJson29SubmissionDate() throws ParseException, IOException {
//
//    requestJson.setSubmissionDate("2019-04-11T10:02:07.098Z");
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display submission date", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Application submitted</td>\n" +
//                                        "<td class=\"table-td-value\">11/04/19</td>"));
//  }
//
//  @Test
//  public void testTransformJson30WorkedAbroadAsYes() throws ParseException, IOException {
//
//    dataCapture.setWorkOverseas("yes");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display worked abroad as Yes", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Worked abroad in the last 5 years</td>\n" +
//                                        "<td class=\"table-td-value\">Yes</td>"));
//  }
//
//  @Test
//  public void testTransformJson31WorkedAbroadAsNo() throws ParseException, IOException {
//
//    dataCapture.setWorkOverseas("no");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display worked abroad as No", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Worked abroad in the last 5 years</td>\n" +
//                                        "<td class=\"table-td-value\">No</td>"));
//  }
//
//  @Test
//  public void testTransformJson32LivedAbroadAsYes() throws ParseException, IOException {
//
//    dataCapture.setMilitaryOverseas("yes");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display lived abroad as Yes", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Lived abroad with family member in armed forces in the last 12 weeks</td>\n" +
//                                        "<td class=\"table-td-value\">Yes</td>"));
//  }
//
//  @Test
//  public void testTransformJson33LivedAbroadAsNo() throws ParseException, IOException {
//
//    dataCapture.setMilitaryOverseas("no");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display lived abroad as No", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Lived abroad with family member in armed forces in the last 12 weeks</td>\n" +
//                                        "<td class=\"table-td-value\">No</td>"));
//  }
//
//  @Test
//  public void testTransformJson36CoronavirusAsNo() throws ParseException, IOException {
//
//    dataCapture.setCoronavirus("no");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display coronavirus answer as No", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Coronavirus the main reason for claim</td>\n" +
//                                        "<td class=\"table-td-value\">No</td>"));
//  }
//
//  @Test
//  public void testTransformJson38CoronavirusAsYesAndOtherHealthCondition() throws ParseException, IOException {
//
//    dataCapture.setCoronavirus("yes");
//    dataCapture.setCoronavirusReason("high-risk");
//    dataCapture.setCoronavirusDate("2019-02-02");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display coronavirus answer", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Coronavirus the main reason for claim</td>\n" +
//                                        "<td class=\"table-td-value\">Yes</td>"));
//    assertThat("should display coronavirus date", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Date affected by coronavirus</td>\n" +
//                                        "<td class=\"table-td-value\">02/02/19</td>"));
//    assertThat("should display other condition answer", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Other health conditions</td>\n" +
//                                        "<td class=\"table-td-value\">Yes</td>"));
//    assertThat("should display other conditions", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Conditions</td>\n" +
//                                        "<td class=\"table-td-value\">fever<br/>Started: 01/01/19<br/></td>"));
//  }
//
//
//  @Test
//  public void testTransformJson39CoronavirusAsHighRiskWithShielding() throws ParseException, IOException {
//
//    String expectedResult =    "<h2>Health conditions</h2>\n" +
//                                   "    <tr style=\"display:none;\">\n" +
//                                   "        <td class=\"table-td-caption\">Coronavirus the main reason for claim</td>\n" +
//                                   "        <td class=\"table-td-value\">${coronaVirus}</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:table-row;\">\n" +
//                                   "        <td class=\"table-td-caption\">Reason for claiming because of coronavirus</td>\n" +
//                                   "        <td class=\"table-td-value\">high risk description</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:none;\">\n" +
//                                   "        <td class=\"table-td-caption\">How affected by coronavirus if other</td>\n" +
//                                   "        <td class=\"table-td-value\">${coronaVirusReasonOther}</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:table-row;\">\n" +
//                                   "        <td class=\"table-td-caption\">Received letter or text from government or NHS if shielding</td>\n" +
//                                   "        <td class=\"table-td-value\">Yes</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:table-row;\">\n" +
//                                   "        <td class=\"table-td-caption\">Date affected by coronavirus</td>\n" +
//                                   "        <td class=\"table-td-value\">02/02/19</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:none;\">\n" +
//                                   "        <td class=\"table-td-caption\">Other health conditions</td>\n" +
//                                   "        <td class=\"table-td-value\">${otherCondition}</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:table-row;\">\n" +
//                                   "        <td class=\"table-td-caption\">Conditions</td>\n" +
//                                   "        <td class=\"table-td-value\">fever<br/>Started: 01/01/19<br/></td>\n" +
//                                   "    </tr>";
//
//    dataCapture.setCoronavirus(null);
//    dataCapture.setOtherHealthCondition(null);
//    dataCapture.setCoronavirusReason("high-risk");
//    dataCapture.setCoronavirusDate("2019-02-02");
//    dataCapture.setCoronavirusShielding("yes");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display coronavirus reason as high risk with shielding", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString(expectedResult.replaceAll("    ", "")));
//  }
//
//  @Test
//  public void testTransformJson40CoronavirusAsHighRiskWithNotShielding() throws ParseException, IOException {
//
//    String expectedResult =    "    <tr style=\"display:table-row;\">\n" +
//                                   "        <td class=\"table-td-caption\">Received letter or text from government or NHS if shielding</td>\n" +
//                                   "        <td class=\"table-td-value\">No</td>";
//
//    dataCapture.setCoronavirus(null);
//    dataCapture.setOtherHealthCondition(null);
//    dataCapture.setCoronavirusReason("high-risk");
//    dataCapture.setCoronavirusDate("2019-02-02");
//    dataCapture.setCoronavirusShielding("no");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display coronavirus shielding", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString(expectedResult.replaceAll("    ", "")));
//  }
//
//  @Test
//  public void testTransformJson41CoronavirusAsAsymptomaticWithOtherConditions() throws ParseException, IOException {
//
//    String expectedResult =    "    <h2>Health conditions</h2>\n" +
//                                   "    <tr style=\"display:none;\">\n" +
//                                   "        <td class=\"table-td-caption\">Coronavirus the main reason for claim</td>\n" +
//                                   "        <td class=\"table-td-value\">${coronaVirus}</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:table-row;\">\n" +
//                                   "        <td class=\"table-td-caption\">Reason for claiming because of coronavirus</td>\n" +
//                                   "        <td class=\"table-td-value\">asymptomatic risk description</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:none;\">\n" +
//                                   "        <td class=\"table-td-caption\">How affected by coronavirus if other</td>\n" +
//                                   "        <td class=\"table-td-value\">${coronaVirusReasonOther}</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:none;\">\n" +
//                                   "        <td class=\"table-td-caption\">Received letter or text from government or NHS if shielding</td>\n" +
//                                   "        <td class=\"table-td-value\">${coronaVirusShield}</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:table-row;\">\n" +
//                                   "        <td class=\"table-td-caption\">Date affected by coronavirus</td>\n" +
//                                   "        <td class=\"table-td-value\">02/02/19</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:table-row;\">\n" +
//                                   "        <td class=\"table-td-caption\">Other health conditions</td>\n" +
//                                   "        <td class=\"table-td-value\">Yes</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:table-row;\">\n" +
//                                   "        <td class=\"table-td-caption\">Conditions</td>\n" +
//                                   "        <td class=\"table-td-value\">fever<br/>Started: 01/01/19<br/></td>\n" +
//                                   "    </tr>";
//
//    dataCapture.setCoronavirus(null);
//    dataCapture.setOtherHealthCondition("yes");
//    dataCapture.setCoronavirusReason("asymptomatic");
//    dataCapture.setCoronavirusReasonDesc("asymptomatic risk description");
//    dataCapture.setCoronavirusDate("2019-02-02");
//    dataCapture.setCoronavirusShielding(null);
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display asymptomatic as coronavirus reason with other conditions",
//        resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString(expectedResult.replaceAll("    ", "")));
//  }
//
//  @Test
//  public void testTransformJson42CoronavirusAsAsymptomaticWithNoOtherConditions() throws ParseException, IOException {
//
//    String expectedResult =    "    <h2>Health conditions</h2>\n" +
//                                   "    <tr style=\"display:none;\">\n" +
//                                   "        <td class=\"table-td-caption\">Coronavirus the main reason for claim</td>\n" +
//                                   "        <td class=\"table-td-value\">${coronaVirus}</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:table-row;\">\n" +
//                                   "        <td class=\"table-td-caption\">Reason for claiming because of coronavirus</td>\n" +
//                                   "        <td class=\"table-td-value\">asymptomatic risk description</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:none;\">\n" +
//                                   "        <td class=\"table-td-caption\">How affected by coronavirus if other</td>\n" +
//                                   "        <td class=\"table-td-value\">${coronaVirusReasonOther}</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:none;\">\n" +
//                                   "        <td class=\"table-td-caption\">Received letter or text from government or NHS if shielding</td>\n" +
//                                   "        <td class=\"table-td-value\">${coronaVirusShield}</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:table-row;\">\n" +
//                                   "        <td class=\"table-td-caption\">Date affected by coronavirus</td>\n" +
//                                   "        <td class=\"table-td-value\">02/02/19</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:table-row;\">\n" +
//                                   "        <td class=\"table-td-caption\">Other health conditions</td>\n" +
//                                   "        <td class=\"table-td-value\">No</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:none;\">\n" +
//                                   "        <td class=\"table-td-caption\">Conditions</td>\n" +
//                                   "        <td class=\"table-td-value\">${conditions}</td>\n" +
//                                   "    </tr>";
//    dataCapture.setCoronavirus(null);
//    dataCapture.setOtherHealthCondition("no");
//    dataCapture.setCoronavirusReason("asymptomatic");
//    dataCapture.setCoronavirusReasonDesc("asymptomatic risk description");
//    dataCapture.setCoronavirusDate("2019-02-02");
//    dataCapture.setCoronavirusShielding(null);
//    dataCapture.setConditionsList(null);
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("display asymptomatic as coronavirus reason with no other conditions",
//        resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString(expectedResult.replaceAll("    ", "")));
//  }
//
//  @Test
//  public void testTransformJson43CoronavirusAsOtherWithOtherConditions() throws ParseException, IOException {
//
//    String expectedResult =    "    <h2>Health conditions</h2>\n" +
//                                   "    <tr style=\"display:none;\">\n" +
//                                   "        <td class=\"table-td-caption\">Coronavirus the main reason for claim</td>\n" +
//                                   "        <td class=\"table-td-value\">${coronaVirus}</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:table-row;\">\n" +
//                                   "        <td class=\"table-td-caption\">Reason for claiming because of coronavirus</td>\n" +
//                                   "        <td class=\"table-td-value\">Other reason</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:table-row;\">\n" +
//                                   "        <td class=\"table-td-caption\">How affected by coronavirus if other</td>\n" +
//                                   "        <td class=\"table-td-value\">other risk description</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:none;\">\n" +
//                                   "        <td class=\"table-td-caption\">Received letter or text from government or NHS if shielding</td>\n" +
//                                   "        <td class=\"table-td-value\">${coronaVirusShield}</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:table-row;\">\n" +
//                                   "        <td class=\"table-td-caption\">Date affected by coronavirus</td>\n" +
//                                   "        <td class=\"table-td-value\">02/02/19</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:table-row;\">\n" +
//                                   "        <td class=\"table-td-caption\">Other health conditions</td>\n" +
//                                   "        <td class=\"table-td-value\">Yes</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:table-row;\">\n" +
//                                   "        <td class=\"table-td-caption\">Conditions</td>\n" +
//                                   "        <td class=\"table-td-value\">fever<br/>Started: 01/01/19<br/></td>\n" +
//                                   "    </tr>";
//    dataCapture.setCoronavirus(null);
//    dataCapture.setOtherHealthCondition("yes");
//    dataCapture.setCoronavirusReason("other");
//    dataCapture.setCoronavirusReasonDesc("other risk description");
//    dataCapture.setCoronavirusDate("2019-02-02");
//    dataCapture.setCoronavirusShielding(null);
//    dataCapture.setConditionsList(conditionsList);
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("display other as coronavirus reason with other conditions", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString(expectedResult.replaceAll("    ", "")));
//  }
//
//  @Test
//  public void testTransformJson44CoronavirusAsOtherWithNoOtherConditions() throws ParseException, IOException {
//
//    String expectedResult =    "    <h2>Health conditions</h2>\n" +
//                                   "    <tr style=\"display:none;\">\n" +
//                                   "        <td class=\"table-td-caption\">Coronavirus the main reason for claim</td>\n" +
//                                   "        <td class=\"table-td-value\">${coronaVirus}</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:table-row;\">\n" +
//                                   "        <td class=\"table-td-caption\">Reason for claiming because of coronavirus</td>\n" +
//                                   "        <td class=\"table-td-value\">Other reason</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:table-row;\">\n" +
//                                   "        <td class=\"table-td-caption\">How affected by coronavirus if other</td>\n" +
//                                   "        <td class=\"table-td-value\">other risk description</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:none;\">\n" +
//                                   "        <td class=\"table-td-caption\">Received letter or text from government or NHS if shielding</td>\n" +
//                                   "        <td class=\"table-td-value\">${coronaVirusShield}</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:table-row;\">\n" +
//                                   "        <td class=\"table-td-caption\">Date affected by coronavirus</td>\n" +
//                                   "        <td class=\"table-td-value\">02/02/19</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:table-row;\">\n" +
//                                   "        <td class=\"table-td-caption\">Other health conditions</td>\n" +
//                                   "        <td class=\"table-td-value\">No</td>\n" +
//                                   "    </tr>\n" +
//                                   "    <tr style=\"display:none;\">\n" +
//                                   "        <td class=\"table-td-caption\">Conditions</td>\n" +
//                                   "        <td class=\"table-td-value\">${conditions}</td>\n" +
//                                   "    </tr>";
//
//    dataCapture.setCoronavirus(null);
//    dataCapture.setOtherHealthCondition("no");
//    dataCapture.setCoronavirusReason("other");
//    dataCapture.setCoronavirusReasonDesc("other risk description");
//    dataCapture.setCoronavirusDate("2019-02-02");
//    dataCapture.setCoronavirusShielding(null);
//    dataCapture.setConditionsList(null);
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("display other as coronavirus reason with no other conditions",
//        resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString(expectedResult.replaceAll("    ", "")));
//  }
//
//  @Test
//  public void testTransformJson45WithNoFitToWork() throws ParseException, IOException {
//
//    dataCapture.setBackToWork(null);
//    dataCapture.setBackToWorkDate(null);
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display fit to work as No", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.not(CoreMatchers.containsString("<td class=\"table-td-caption\">Return to work date</td>\n<td class=\"table-td-value\">No</td>")));
//
//  }
//
//  @Test
//  public void testTransformJson46WithNoClaimEndDate() throws ParseException, IOException {
//
//    dataCapture.setClaimEnd(null);
//    dataCapture.setClaimEndDate(null);
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display claim end as Dont know", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.not(CoreMatchers.containsString("<td class=\"table-td-caption\">Claim end date</td>\n<td class=\"table-td-value\">Don't know</td>")));
//
//  }
//
//  @Test
//  public void testTransformJson47WithDontKnowClaimEndDate() throws ParseException, IOException {
//
//    dataCapture.setClaimEnd("no");
//    dataCapture.setClaimEndDate(null);
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display claim end date as Dont know", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Claim end date</td>\n<td class=\"table-td-value\">Don't know</td>"));
//
//  }
//
//  @Test
//  public void testTransformJson48WithClaimEndDate() throws ParseException, IOException {
//
//    dataCapture.setClaimEnd("yes");
//    dataCapture.setClaimEndDate("2019-01-01");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display claim end date", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Claim end date</td>\n<td class=\"table-td-value\">01/01/19</td>"));
//
//  }
//
//  @Test
//  public void testTransformJson49WithUniversalCreditAsYes() throws ParseException, IOException {
//
//    dataCapture.setUniversalCredit("yes");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display universal credit as Yes", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Gets or applied for Universal Credit</td>\n<td class=\"table-td-value\">Yes</td>"));
//
//  }
//
//  @Test
//  public void testTransformJson50WithUniversalCreditAsNo() throws ParseException, IOException {
//
//    dataCapture.setUniversalCredit("no");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display universal credit as No", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Gets or applied for Universal Credit</td>\n<td class=\"table-td-value\">No</td>"));
//
//  }
//
//  @Test
//  public void testTransformJson51WithUniversalCreditNotPresent() throws ParseException, IOException {
//
//    dataCapture.setUniversalCredit(null);
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should not display universal credit", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Gets or applied for Universal Credit</td>\n<td class=\"table-td-value\">${universalCredit}</td>"));
//
//  }
//
//  @Test
//  public void testTransformJson52StatutoryPayment() throws ParseException, IOException {
//
//    dataCapture.setStatutoryPayOther("maternity");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display maternity statutory pay", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Other statutory payments</td>\n" +
//                                        "<td class=\"table-td-value\">Statutory Maternity Pay</td>\n"));
//
//  }
//
//  @Test(expected=IllegalArgumentException.class)
//  public void testTransformJson53StatutoryPaymentWithWrongValue() throws ParseException, IOException {
//
//    dataCapture.setStatutoryPayOther("wrong");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//  }
//
//  @Test
//  public void testTransformJson54ClaimStartDateAfterSspAsYes() throws ParseException, IOException {
//
//    dataCapture.setClaimStartDateAfterSsp("yes");
//    dataCapture.setDayAfterSspRecentEnd("2019-02-03");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display claim after ssp date", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<tr style=\"display:table-row;\">\n" +
//                                              "<td class=\"table-td-caption\">Claim from 03/02/19</td>\n" +
//                                              "<td class=\"table-td-value\">Yes</td>\n" +
//                                              "</tr>\n" +
//                                              "<tr>\n" +
//                                              "<td class=\"table-td-caption\">Claim start date</td>\n" +
//                                              "<td class=\"table-td-value\">03/02/19</td>"));
//
//  }
//
//  @Test
//  public void testTransformJson55ClaimStartDateAfterAfterSspAsNo() throws ParseException, IOException {
//
//    dataCapture.setClaimStartDateAfterSsp("no");
//    dataCapture.setDayAfterSspRecentEnd("2019-02-03");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display claim after ssp date", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<tr style=\"display:table-row;\">\n" +
//                                              "<td class=\"table-td-caption\">Claim from 03/02/19</td>\n" +
//                                              "<td class=\"table-td-value\">No</td>\n" +
//                                              "</tr>\n" +
//                                              "<tr>\n" +
//                                              "<td class=\"table-td-caption\">Claim start date</td>\n" +
//                                              "<td class=\"table-td-value\">01/01/19</td>"));
//
//  }
//
//  @Test
//  public void testTransformJson56ClaimStartDateAfterAfterSspAsNull() throws ParseException, IOException {
//
//    dataCapture.setClaimStartDateAfterSsp(null);
//    dataCapture.setDayAfterSspRecentEnd(null);
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should not display claim after ssp date", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<tr style=\"display:none;\">\n" +
//                                              "<td class=\"table-td-caption\">Claim from ${dayAfterSspRecentEndDate}</td>\n" +
//                                              "<td class=\"table-td-value\">${claimStartDateAfterSsp}</td>\n" +
//                                              "</tr>\n" +
//                                              "<tr>\n" +
//                                              "<td class=\"table-td-caption\">Claim start date</td>\n" +
//                                              "<td class=\"table-td-value\">01/01/19</td>"));
//  }
//
//  @Test
//  public void testTransformJson54NoWelshPostCode() throws ParseException, IOException {
//
//    dataCapture.setWelshPostcode("no");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should not display language preference section", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<table style=\"display:none;\">\n" +
//                                        "<h2 style=\"display:none;\">Language preference</h2>\n"));
//  }
//
//  @Test
//  public void testTransformJson55WelshPostCode() throws ParseException, IOException {
//
//    dataCapture.setWelshPostcode("yes");
//    dataCapture.setLangPrefWriting("Welsh");
//    dataCapture.setLangPrefSpeaking("English");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display language preference section", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<table style=\"display:row;\">\n" +
//                                        "<h2 style=\"display:row;\">Language preference</h2>\n"));
//    assertThat("should display language preference writing", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Writing</td>\n" +
//                                        "<td class=\"table-td-value\">Welsh</td>\n"));
//    assertThat("should display language preference speaking", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Speaking</td>\n" +
//                                        "<td class=\"table-td-value\">English</td>\n"));
//  }
//
//  @Test
//  public void testTransformJson56NoPension() throws ParseException, IOException {
//
//    dataCapture.setPensionQuestion("no");
//    dataCapture.setPensionInherit(null);
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should not display pension inherit section", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//            CoreMatchers.containsString("<tr style=\"display:none;}\">\n" +
//                    "<td class=\"table-td-caption\">Getting pension or annuity payments inherited from someone who died</td>"));
//  }
//
//  @Test
//  public void testTransformJson57WithPensionInherit() throws ParseException, IOException {
//
//    dataCapture.setPensionQuestion("yes");
//    dataCapture.setPensionInherit("yes");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display pension inherit section", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//            CoreMatchers.containsString("<tr style=\"display:table-row;}\">\n" +
//                    "<td class=\"table-td-caption\">Getting pension or annuity payments inherited from someone who died</td>\n" +
//                    "<td class=\"table-td-value\">Yes</td>\n"));
//  }
//
//  @Test
//  public void testDoBWithAlternateDateFormat() throws ParseException, IOException {
//
//    applicant.setDateOfBirth("1981-Mar-01");
//    requestJson.setApplicant(applicant);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display applicant date of birth", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Date of birth</td>\n" +
//                                        "<td class=\"table-td-value\">01/03/81</td>"));
//  }
//
//  @Test
//  public void testClaimStartDateWithAlternateDateFormat() throws ParseException, IOException {
//
//
//    dataCapture.setClaimStartDate("2020-Feb-02");
//    requestJson.setDataCapture(dataCapture);
//    resolvedHtml = transformRequest(requestJson);
//
//    assertThat("should display claim start date", resolvedHtml.replaceAll("\r", "").replaceAll("  ", ""),
//        CoreMatchers.containsString("<td class=\"table-td-caption\">Claim start date</td>\n" +
//                                        "<td class=\"table-td-value\">02/02/20</td>"));
//  }

}
