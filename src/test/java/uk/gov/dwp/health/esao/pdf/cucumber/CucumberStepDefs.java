package uk.gov.dwp.health.esao.pdf.cucumber;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.Rule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class CucumberStepDefs {
  private static final Logger LOG = LoggerFactory.getLogger(CucumberStepDefs.class.getName());
  private CloseableHttpResponse response;
  private CloseableHttpClient httpClient;
  private String payload;
  @Rule
  public WireMockRule htmlToPDFService = new WireMockRule(wireMockConfig().port(6677));

  @Before
  public void setup() {

    httpClient = HttpClients.createDefault();
    htmlToPDFService.stubFor(post(urlEqualTo("/generatePdf"))
                                     .willReturn(aResponse()
                                                         .withBody("valid pdf content")
                                                         .withStatus(200)
                                     ));

    htmlToPDFService.start();
    LOG.info("htmlToPDFService started");
  }

  @After
  public void tearDown() {
    htmlToPDFService.stop();
  }

  @When("^I hit \"([^\"]*)\" with an empty POST request$")
  public void iHitWithAnEmptyPostRequest(String url) throws Throwable {
    performHttpPostWithUriOf(url, "");
  }

  @Then("^I should get a (\\d+) response$")
  public void iShouldGetAResponse(int status) {
    assertThat("response codes do not match", response.getStatusLine().getStatusCode(), is(equalTo(status)));
  }

  @Then("^The response payload contains the following text : \"([^\"]*)\"$")
  public void theResponsePayloadContainsTheFollowingText(String searchText) {
    assertNotNull("cannot be null", payload);
    assertThat(String.format("payload should contain %s", searchText), payload, containsString(searchText));
  }


  @When("^I hit \"([^\"]*)\" with the a JSON string of \"([^\"]*)\"$")
  public void iHitWithTheAJSONStringOf(String url, String json) throws Throwable {
    performHttpPostWithUriOf(url, json);
  }


  @When("^I hit the service url \"([^\"]*)\" with the following json body taken from file \"([^\"]*)\" with msg_id \"([^\"]*)\" and submission time of now plus (\\d+) days$")
  public void iHitTheServiceUrlWithTheFollowingJsonBodyTakenFromFile(String url, String fileRef, String msgId, int daysOffset) throws IOException {
    String isoDate = DateTimeFormatter.ISO_INSTANT.format(Instant.now().plus(Duration.ofDays(daysOffset)));
    String json = String.format(FileUtils.readFileToString(new File(fileRef)), msgId, isoDate);
    performHttpPostWithUriOf(url, json);
  }

  @When("^I hit the service url \"([^\"]*)\" with the invalid content$")
  public void iHitTheServiceUrlWithTheInvalidContent(String url, Map<String, String> jsonValues) throws Throwable {
    String jsonRequestBody = buildJsonBody(jsonValues);
    performHttpPostWithUriOf(url, jsonRequestBody);
  }

  @When("^I hit the service url \"([^\"]*)\" with the invalid schema$")
  public void iHitTheServiceUrlWithTheInvalidSchema(String url) throws Throwable {
    String json = "{\n" +
                          "      \"msg_id\": \"someId\",\n" +
                          "      \"ref\": null\n" +
                          "    }";
    performHttpPostWithUriOf(url, json);
  }

  @When("^I hit the service url \"([^\"]*)\" with the following json body$")
  public void iHitTheServiceUrlWithTheFollowingJsonBody(String url, Map<String, String> jsonValues) throws Throwable {
    String jsonRequestBody = buildJsonBody(jsonValues);
    performHttpPostWithUriOf(url, jsonRequestBody);
  }

  private String buildJsonBody(Map<String, String> jsonValues) {
    StringBuilder builder = new StringBuilder();
    builder.append("{");
    String delimiter = "";
    for (Map.Entry<String, String> jsonKeyValue : jsonValues.entrySet()) {
      if (!jsonKeyValue.getKey().isEmpty()) {
        builder.append(delimiter);
        builder.append(String.format("\"%s\":", jsonKeyValue.getKey()));
        builder.append(jsonKeyValue.getValue());
        delimiter = ",";
      }
    }
    builder.append("}");
    return builder.toString();
  }

  private void performHttpPostWithUriOf(String uri, String body) throws IOException {
    HttpPost httpUriRequest = new HttpPost(uri);

    if ((body != null) && (!body.isEmpty())) {
      HttpEntity entity = new StringEntity(body);
      httpUriRequest.setEntity(entity);
    }

    response = httpClient.execute(httpUriRequest);
    HttpEntity responseEntity = response.getEntity();
    payload = EntityUtils.toString(responseEntity);
  }
}
