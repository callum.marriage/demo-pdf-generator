package uk.gov.dwp.health.esao.pdf;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.junit.ClassRule;
import org.junit.runner.RunWith;
import uk.gov.dwp.health.esao.pdf.application.PdfGeneratorApplication;
import uk.gov.dwp.health.esao.pdf.application.PdfGeneratorConfiguration;

import static io.dropwizard.testing.ResourceHelpers.resourceFilePath;

@RunWith(Cucumber.class)
@SuppressWarnings({"squid:S2187", "squid:S1118", "squid:S4042"})
// no tests needed to kick of cucumber, private constructor and use of File.delete()
@CucumberOptions(plugin = "json:target/cucumber-report.json", tags = {})
public class RunCukesTest {

  @ClassRule
  public static final DropwizardAppRule<PdfGeneratorConfiguration> RULE = new DropwizardAppRule<>(PdfGeneratorApplication.class, resourceFilePath("test.yml"));
}