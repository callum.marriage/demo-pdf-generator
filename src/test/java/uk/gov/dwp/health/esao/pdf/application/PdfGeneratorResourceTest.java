package uk.gov.dwp.health.esao.pdf.application;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import uk.gov.dwp.health.esao.pdf.PdfGeneratorResource;
import uk.gov.dwp.health.esao.pdf.application.handlers.HtmlToPdfaHandler;
import uk.gov.dwp.health.esao.pdf.application.util.DataTransformation;
import uk.gov.dwp.health.esao.shared.models.RequestJson;
import uk.gov.dwp.health.utilities.JsonObjectSchemaValidation;

import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PdfGeneratorResourceTest {
  private static final File SCHEMA_FILE = new File("src/test/schemeDoc.yml");
  private static final String SCHEMA_ENTRY = "testInput";
/*
  @Mock
  private JsonObjectSchemaValidation schemaValidation;

  @Mock
  private PdfGeneratorConfiguration configuration;

  @Mock
  private HtmlToPdfaHandler htmlHandler;

  @Mock
  DataTransformation dataTransformation;

  @Captor
  private ArgumentCaptor<RequestJson> transformCapture;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    when(configuration.getSchemaValidationFile()).thenReturn(SCHEMA_FILE);
    when(configuration.getSchemaEntry()).thenReturn(SCHEMA_ENTRY);
  }

  @Test
  public void schemaValidationSuccess() throws ParseException, IOException {

    String incomingJson = IOUtils.toString(getClass().getResourceAsStream("/test-case.json"));
    String pdfFile = "i-am-pdf";

    when(htmlHandler.generateBase64PdfFromHtml(anyString())).thenReturn(Base64.encodeBase64String(pdfFile.getBytes()));

    PdfGeneratorResource instance = new PdfGeneratorResource(configuration, schemaValidation, htmlHandler, dataTransformation);
    Map<String, String> valuesMap = new HashMap<>();
    when(dataTransformation.transformData(any())).thenReturn(valuesMap);

    Response response = instance.generatePdf(incomingJson);

    verify(schemaValidation, times(1)).validateJsonDocumentWithFile(eq(incomingJson), eq(SCHEMA_ENTRY), eq(SCHEMA_FILE));
    verify(htmlHandler, times(1)).generateBase64PdfFromHtml(anyString());
    verify(dataTransformation, times(1)).transformData(any());
    verify(dataTransformation, times(1)).transformData(transformCapture.capture());

    assertThat(transformCapture.getValue().getDataCapture().getConditionsList().get(0).getName(), is(equalTo("neural hard drive &#230;")));
    assertThat(transformCapture.getValue().getDataCapture().getMedicalCentre().getName(), is(equalTo("Lang &#38; Collier Emmerich")));
    assertThat(transformCapture.getValue().getDataCapture().getMedicalCentre().getAddress().getPremises(), is(equalTo("na &#60;")));
    assertThat(transformCapture.getValue().getDataCapture().getMedicalCentre().getAddress().getLines().get(1), is(equalTo("na &#62;")));
    assertThat(transformCapture.getValue().getDataCapture().getMedicalCentre().getAddress().getLines().get(0), is(equalTo("01301 Brown Turnpike ]]&#62;")));
    assertThat(transformCapture.getValue().getDataCapture().getMedicalCentre().getDoctorName(), is(equalTo("Dr. -- Tobin Dach")));

    assertThat(response.getStatusInfo().getStatusCode(), is(equalTo(HttpStatus.SC_OK)));
    assertThat(response.getEntity(), is(equalTo(pdfFile.getBytes())));
  }

  @Test
  public void schemaValidationFailure() throws IOException {
    String incomingJson = "{\"key\":\"value\"}";

    doThrow(new IOException("i-am-an-exception")).when(schemaValidation).validateJsonDocumentWithFile(eq(incomingJson), eq(SCHEMA_ENTRY), eq(SCHEMA_FILE));

    PdfGeneratorResource instance = new PdfGeneratorResource(configuration, schemaValidation, htmlHandler, dataTransformation);
    Response response = instance.generatePdf(incomingJson);

    verify(schemaValidation, times(1)).validateJsonDocumentWithFile(eq(incomingJson), eq(SCHEMA_ENTRY), eq(SCHEMA_FILE));
    verifyZeroInteractions(htmlHandler);
    verifyZeroInteractions(dataTransformation);

    assertThat(response.getStatusInfo().getStatusCode(), is(equalTo(HttpStatus.SC_BAD_REQUEST)));
  }*/
}
