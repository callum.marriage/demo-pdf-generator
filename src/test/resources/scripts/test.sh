#!/bin/bash

testJson1='{
   "msg_id":"esa.submission.new",
   "ref":"R5MG51",
   "date_submitted":"2019-04-11T10:02:07.098Z",
   "applicant":{
      "forenames":"Coy",
      "surname":"Labadie",
      "dob":"1981-04-07",
      "residence_address":{
         "lines":[
            "182 Granville Neck",
            "",
            ""
         ],
         "premises":"",
         "postcode":"LS1 1DJ"
      },
      "contact_options":[
         {
            "method":"telmobile",
            "data":"(944) 060-1089",
            "preferred":true
         }
      ]
   },
   "data_capture":{
      "language":"en",
      "coronavirus":"yes",
      "coronavirus_date":"2020-02-02",
      "other_health_condition":"yes",
      "conditions":[
         {
            "name":"auxiliary application",
            "start_date":"2003-02-01"
         },
         {
            "name":"1080p monitor",
            "start_date":"2003-02-01"
         },
         {
            "name":"multi-byte feed",
            "start_date":"2003-02-01"
         }
      ],
      "medical_centre":{
         "name":"Renner, Turcotte and Cummerata",
         "tel":"(195) 473-4355",
         "address":{
            "lines":[
               "30501 Edwin Via",
               "",
               "New Frankie"
            ],
            "premises":"",
            "postcode":"LS1 1DJ"
         },
         "doctor":"Dr. Ola Maggio"
      },
      "statutory_pay_other":"maternity",
      "nino":"AS123123D",
      "bank_account_name":"Clement Runolfsson",
      "bank_name":"Olson - Bauch",
      "bank_sort_code":"011934",
      "bank_account_number":"12345678",
      "claim_start_date":"2019-03-21",
      "back_to_work":"yes",
      "back_to_work_date":"2019-05-11",
      "pregnant":"yes",
      "due_date":"2019-07-11",
      "severe_condition":"yes",
      "ds1500_report":"yes",
      "hospital_inpatient":"yes",
      "hospital_name":"Reichel Group",
      "hospital_ward":"Murray - Johnson",
      "hospital_admission_date":"2019-01-11",
      "doc_share_with_dwp":"yes",
      "dwp_share_with_doc":"yes",
      "work_overseas": "yes",
      "military_overseas": "no",
      "voluntary_work_question":"yes",
      "voluntary_work":[
         {
            "organisation_name":"Collier LLC",
            "organisation_address":{
               "lines":[
                  "899 Francisca Course",
                  "",
                  ""
               ],
               "premises":"",
               "postcode":"LS1 1DJ"
            },
            "role":"Voluptas id aut. Laudantium debitis et voluptatibus explicabo maxime et sunt accusantium. In id ut. Unde ullam quis blanditiis assumenda sed qui.",
            "same_hours":"yes",
            "hours":"1"
         },
         {
            "organisation_name":"Okuneva - Stehr",
            "organisation_address":{
               "lines":[
                  "96561 Erik Prairie",
                  "",
                  ""
               ],
               "premises":"",
               "postcode":"LS1 1DJ"
            },
            "role":"Nostrum magnam dolores ducimus repellendus nemo rerum est eligendi et. Id ut accusamus qui voluptatem tempora ipsam eos est adipisci. Et eos fugit est reiciendis molestiae velit dignissimos labore.",
            "same_hours":"yes",
            "hours":"1"
         },
         {
            "organisation_name":"Prohaska - Stoltenberg",
            "organisation_address":{
               "lines":[
                  "8272 Stuart Squares",
                  "",
                  ""
               ],
               "premises":"",
               "postcode":"LS1 1DJ"
            },
            "role":"Non aut eveniet saepe velit qui ex earum. Porro repellendus sunt molestiae. Rem non vel exercitationem qui aut placeat nihil. Dignissimos ad qui ea perspiciatis accusamus. Error rerum repudiandae repellendus maxime eius voluptas aut voluptate laborum.",
            "same_hours":"yes",
            "hours":"1"
         }
      ],
      "employment_question":"yes",
      "employments":[
         {
            "job_title":"Senior Mobility Engineer",
            "employer_name":"Crooks - Bahringer",
            "employer_tel":"(796) 071-9247",
            "employer_address":{
               "lines":[
                  "26249 Duane Well",
                  "",
                  "New Rozellaberg"
               ],
               "premises":"",
               "postcode":"LS1 1DJ"
            },
            "employment_status":[
               "employee",
               "subContractor"
            ],
            "off_sick":"yes",
            "last_worked_date":"2019-03-21"
         }
      ],
      "ssp":"no",
      "ssp_recent":"yes",
      "ssp_recent_end":"2019-05-02",
      "pension_question":"yes",
      "pensions":[
         {
            "pension_provider":"Durgan, Rodriguez and Klocko",
            "provider_ref":"1-427-160-3863",
            "provider_tel":"(691) 458-5299",
            "provider_address":{
               "lines":[
                  "705 Ruth Route",
                  "",
                  "Freidaport"
               ],
               "premises":"",
               "postcode":"LS1 1DJ"
            },
            "start_date":"2018-04-11",
            "deductions":"yes",
            "amount_gross":"1423",
            "frequency":"every4Weeks",
            "inherited":"yes",
            "amount_net":"423",
            "deduction_details":[
               {
                  "amount":"123",
                  "detail":"Fuga explicabo blanditiis corporis."
               },
               {
                  "amount":"825",
                  "detail":"Aliquam asperiores ea corrupti."
               },
               {
                  "amount":"342",
                  "detail":"Dolores est autem veniam."
               }
            ]
         }
      ],
      "insurance_question":"yes",
      "insurance":[
         {
            "insurance_provider":"Rosenbaum - McKenzie",
            "provider_ref":"1-242-684-1691",
            "provider_tel":"(735) 568-9708",
            "provider_address":{
               "lines":[
                  "89272 Tyrell Brooks",
                  "",
                  "Zemlakstad"
               ],
               "premises":"",
               "postcode":"LS1 1DJ"
            },
            "amount":"7861",
            "frequency":"quarterly",
            "premiums":"no",
            "employment_end_date":"2019-02-14"
         }
      ],
      "mobile":"yes",
      "welsh_postcode": "yes",
      "lang_pref_writing": "English",
      "lang_pref_speaking": "Welsh"
   },
   "declaration":"By sending this application you confirm my application",
   "tags":[

   ]
}';

testJson2='{
   "msg_id":"esa.submission.new",
   "ref":"QP9L50",
   "date_submitted":"2019-04-11T10:06:05.176Z",
   "applicant":{
      "forenames":"Garrett",
      "surname":"Orn",
      "dob":"1981-04-07",
      "residence_address":{
         "lines":[
            "56579 Brown Spurs",
            "",
            ""
         ],
         "premises":"",
         "postcode":"LS1 1DJ"
      },
      "contact_options":[

      ]
   },
   "data_capture":{
      "language":"en",
      "conditions":[
         {
            "name":"cross-platform capacitor",
            "start_date":"2003-02-01"
         },
         {
            "name":"haptic program",
            "start_date":"2003-02-01"
         },
         {
            "name":"back-end card",
            "start_date":"2003-02-01"
         }
      ],
      "medical_centre":{
         "name":"Eichmann - Howe",
         "tel":"(322) 576-7431",
         "address":{
            "lines":[
               "145 Bogan Port",
               "",
               "Daleshire"
            ],
            "premises":"",
            "postcode":"LS1 1DJ"
         },
         "doctor":"Dr. Lamont Hudson"
      },
      "statutory_pay_other":"none",
      "nino":"AS123123D",
      "bank_account_name":"Cayla Abshire",
      "bank_name":"Hahn Group",
      "bank_sort_code":"011934",
      "bank_account_number":"12345678",
      "claim_start_date":"2019-03-21",
      "back_to_work":"no",
      "pregnant":"no",
      "severe_condition":"no",
      "hospital_inpatient":"no",
      "doc_share_with_dwp":"no",
      "dwp_share_with_doc":"no",
      "work_overseas": "yes",
      "military_overseas": "no",
      "voluntary_work_question":"no",
      "employment_question":"no",
      "ssp_recent":"no",
      "pension_question":"no",
      "insurance_question":"no",
      "mobile":"no",
      "other_number":"no"
   },
   "declaration":"By sending this application you",
   "tags":[

   ]
}';

declare -i k
k=1
set -x

curl -m 10 -XPOST --data "$testJson1" --noproxy '*' http://localhost:9502/generatePdf -o test1-$k.pdf

curl -m 10 -XPOST --data "$testJson2" --noproxy '*' http://localhost:9502/generatePdf -o test2-$k.pdf
