Feature: test the application

  @Generic
  Scenario: POST request to an invalid endpoint
    When I hit "http://localhost:9502/no_endpoint" with an empty POST request
    Then I should get a 404 response
    And The response payload contains the following text : "HTTP 404 Not Found"

  @Generic
  Scenario: POST request to correct endpoint with malformed JSON
    When I hit "http://localhost:9502/generatePdf" with the a JSON string of "{bad, bad, bad}"
    Then I should get a 400 response
    And The response payload contains the following text : "Unable to process request"

  @Generic
  Scenario: POST request to correct endpoint with valid json but invalid schema
    When I hit the service url "http://localhost:9502/generatePdf" with the invalid schema
    Then I should get a 500 response
    And The response payload contains the following text : "Unable to process request"

  @Generic
  Scenario: POST request to correct endpoint with valid json and schema but invalid content
    When I hit the service url "http://localhost:9502/generatePdf" with the invalid content
          | msg_id          | "aaa123"               |
          | date_submitted  | "wrongdate-19T12:46:31Z" |
          | ref             | "somr-ref"             |
          | applicant       | {}                     |
          | data_capture    | {}                     |
          | declaration     | "i declare ok"         |
          | tags            | []                     |
          | id              | "some-id"              |
    Then I should get a 400 response
    And The response payload contains the following text : "Payload contains invalid items"