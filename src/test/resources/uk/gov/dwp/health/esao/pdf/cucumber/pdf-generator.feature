Feature: test the main functionality of the application


  Scenario: POST request to correct endpoint with correct input, should not get valid successful response
    When I hit the service url "http://localhost:9502/generatePdf" with the following json body
      | msg_id          | "aaa123"               |
      | date_submitted  | "2017-02-19T12:46:31Z" |
      | ref             | "somr-ref"             |
      | applicant       | {"forenames":"Bettie", "surname":"Simonis", "dob":"1981-04-07", "residence_address":{ "lines":[ "82909 Waelchi Passage", "", "" ], "premises":"", "postcode":"LS1 1DJ" }, "contact_options":[{"method":"tel", "data":"(962) 472-9013", "preferred":true }]}  |
      | data_capture    | { "language":"en", "conditions":[{"name":"mobile system", "start_date":"2003-02-01"}, {"name":"solid state port", "start_date":"2003-02-01"}], "medical_centre":{ "name":"Lakin - Huel", "tel":"(261) 008-2900", "address":{"lines":[ "987 Myrna Light", "", "West Katlyn"], "premises":"", "postcode":"LS1 1DJ" }, "doctor":"Dr. Henry Kertzmann" }, "statutory_pay_other":"none", "nino":"AS123123D", "doc_share_with_dwp":"yes", "dwp_share_with_doc":"yes", "work_overseas": "yes", "military_overseas": "no", "bank_account_name":"Jedidiah Schroeder", "bank_name":"Murphy and Sons", "bank_sort_code":"011934", "bank_account_number":"12345678", "claim_start_date":"2019-03-05", "ds1500_report":"yes", "pension_question":"no", "insurance_question":"no", "universal_credit":"yes", "ssp": "no", "employment_question": "no", "pregnant": "no", "severe_condition": "no", "hospital_inpatient": "no", "voluntary_work_question": "no"}   |
      | declaration     | "i declare ok"         |
      | tags            | []                     |
      | id              | "some-id"              |
    Then I should get a 200 response

  Scenario: POST request to correct endpoint with correct json format, with empty applicant should result in validation error
    When I hit the service url "http://localhost:9502/generatePdf" with the following json body
      | msg_id          | "aaa123"               |
      | date_submitted  | "2017-02-19T12:46:31Z" |
      | ref             | "somr-ref"             |
      | applicant       | {}                     |
      | data_capture    | { "language":"en", "conditions":[{"name":"mobile system", "start_date":"2003-02-01"}, {"name":"solid state port", "start_date":"2003-02-01"}], "medical_centre":{ "name":"Lakin - Huel", "tel":"(261) 008-2900", "address":{"lines":[ "987 Myrna Light", "", "West Katlyn"], "premises":"", "postcode":"LS1 1DJ" }, "doctor":"Dr. Henry Kertzmann" }, "statutory_pay_other":"none", "nino":"AS123123D", "doc_share_with_dwp":"yes", "dwp_share_with_doc":"yes", "work_overseas": "yes", "military_overseas": "no", "bank_account_name":"Jedidiah Schroeder", "bank_name":"Murphy and Sons", "bank_sort_code":"011934", "bank_account_number":"12345678", "claim_start_date":"2019-03-05"}   |
      | declaration     | "i declare ok"         |
      | tags            | []                     |
      | id              | "some-id"              |
    Then I should get a 400 response
    And The response payload contains the following text : "Payload contains invalid items"

  Scenario: POST request to correct endpoint with correct json format, with empty data_capture should result in validation error
    When I hit the service url "http://localhost:9502/generatePdf" with the following json body
      | msg_id          | "aaa123"               |
      | date_submitted  | "2017-02-19T12:46:31Z" |
      | ref             | "somr-ref"             |
      | applicant       | {"forenames":"Bettie", "surname":"Simonis", "dob":"1981-04-07", "residence_address":{ "lines":[ "82909 Waelchi Passage", "", "" ], "premises":"", "postcode":"LS1 1DJ" }, "contact_options":[{"method":"tel", "data":"(962) 472-9013", "preferred":true }]}  |
      | data_capture    | {}                     |
      | declaration     | "i declare ok"         |
      | tags            | []                     |
      | id              | "some-id"              |
    Then I should get a 400 response
    And The response payload contains the following text : "Payload contains invalid items"

  Scenario: POST request to correct endpoint with missing msg_id, should not get as far as deserialisation
    When I hit the service url "http://localhost:9502/generatePdf" with the following json body
      | bad_bad_field   | "aaa123"               |
      | date_submitted  | "2017-02-19T12:46:31Z" |
      | ref             | "somr-ref"             |
      | applicant       | {}                     |
      | data_capture    | {}                     |
      | declaration     | "i declare ok"         |
      | tags            | []                     |
      | id              | "some-id"              |
    Then I should get a 500 response
    And The response payload contains the following text : "Unable to process request"

  Scenario: POST request to correct endpoint with missing date_submitted, should not get as far as deserialisation
    When I hit the service url "http://localhost:9502/generatePdf" with the following json body
      | msg_id              | "aaa123"               |
      | no_date_submitted   | "2017-02-19T12:46:31Z" |
      | ref                 | "somr-ref"             |
      | applicant           | {}                     |
      | data_capture        | {}                     |
      | declaration         | "i declare ok"         |
      | tags                | []                     |
      | id                  | "some-id"              |
    Then I should get a 500 response
    And The response payload contains the following text : "Unable to process request"

  Scenario: POST request to correct endpoint with invalid date_submitted, should result in validation error
    When I hit the service url "http://localhost:9502/generatePdf" with the following json body
      | msg_id          | "aaa123"               |
      | date_submitted  | "wrong-date-format"    |
      | ref             | "somr-ref"             |
      | applicant       | {}                     |
      | data_capture    | {}                     |
      | declaration     | "i declare ok"         |
      | tags            | []                     |
      | id              | "some-id"              |
    Then I should get a 400 response
    And The response payload contains the following text : "Payload contains invalid items"
