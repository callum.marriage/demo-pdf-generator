package uk.gov.dwp.health.esao.pdf.application;

import io.dropwizard.Application;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import uk.gov.dwp.health.esao.pdf.PdfGeneratorResource;
import uk.gov.dwp.health.esao.pdf.ServiceInfoResource;
import uk.gov.dwp.health.esao.pdf.application.handlers.HtmlToPdfaHandler;
import uk.gov.dwp.health.esao.pdf.application.util.DataTransformation;
import uk.gov.dwp.health.esao.pdf.info.PropertyFileInfoProvider;
import uk.gov.dwp.health.utilities.JsonObjectSchemaValidation;
import uk.gov.dwp.tls.TLSConnectionBuilder;

public class PdfGeneratorApplication extends Application<PdfGeneratorConfiguration> {

  @Override
  protected void bootstrapLogging() {
    // to prevent dropwizard using its own standard logger
  }

  @Override
  public void initialize(Bootstrap<PdfGeneratorConfiguration> bootstrap) {
    bootstrap.setConfigurationSourceProvider(
        new SubstitutingSourceProvider(bootstrap.getConfigurationSourceProvider(),
        new EnvironmentVariableSubstitutor(false)));
  }

  @Override
  public void run(PdfGeneratorConfiguration configuration, Environment environment)
      throws Exception {
    TLSConnectionBuilder connectionBuilder = new TLSConnectionBuilder(
            configuration.getHtmlToPdfTruststoreFile(),
            configuration.getHtmlToPdfTruststorePass(),
            configuration.getHtmlToPdfKeystoreFile(),
            configuration.getHtmlToPdfKeystorePass()
    );

    final HtmlToPdfaHandler htmlToPdfaHandler =
        new HtmlToPdfaHandler(connectionBuilder, configuration);
    final JsonObjectSchemaValidation schemaValidation = new JsonObjectSchemaValidation();
    final DataTransformation dataTransformation = new DataTransformation();

    PdfGeneratorResource instance = new PdfGeneratorResource(configuration, schemaValidation,
        htmlToPdfaHandler, dataTransformation);
    environment.jersey().register(instance);

    if (configuration.isApplicationInfoEnabled()) {
      final ServiceInfoResource infoInstance =
          new ServiceInfoResource(new PropertyFileInfoProvider("application.yml"));
      environment.jersey().register(infoInstance);
    }
  }

  public static void main(String[] args) throws Exception {
    new PdfGeneratorApplication().run(args);
  }
}
