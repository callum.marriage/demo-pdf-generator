package uk.gov.dwp.health.esao.pdf.constants;

public class HTMLConstants {

  private HTMLConstants() {
  }

  public static final String BR_TAG = "<br/>";
  public static final String DISPLAY_TYPE_TABLE_ROW = "table-row";
  public static final String DISPLAY_TYPE_ROW = "row";
  public static final String DISPLAY_TYPE_NONE = "none";
  public static final String YES = "Yes";
  public static final String NO = "No";
  public static final String NOT_SURE = "I'm not sure";
  public static final String DONT_KNOW = "Don't know";
  public static final String BLANK = "";
  public static final String ADD_BR = "addBr";
  public static final String HTML_BR = "<br/><br/>";

  public static final String DISPLAY_LANG_PREF = "displayLangPref";
  public static final String LANG_PREF_WRITING = "langPrefWriting";
  public static final String LANG_PREF_SPEAKING = "langPrefSpeaking";

  public static final String DOING_VOLUNTARY_WORK = "doingVoluntaryWork";
  public static final String DISPLAY_DOING_VOLUNTARY_WORK = "displayDoingVoluntaryWork";
  public static final String VOLUNTARY_WORK_TABLE = "voluntaryWorkTable";
  public static final String CURRENTLY_EMPLOYED = "currentlyEmployed";
  public static final String DISPLAY_CURRENTLY_EMPLOYED = "displayCurrentlyEmployed";
  public static final String EMPLOYMENTS_TABLE = "employmentsTable";
  public static final String GETTING_SSP = "gettingSSP";
  public static final String DISPLAY_GETTING_SSP = "displayGettingSSP";
  public static final String RECEIVED_SSP_IN_LAST12WEEKS = "receivedSSPInLast12Weeks";
  public static final String DISPLAY_RECEIVED_SSP_IN_LAST12WEEKS =
      "displayReceivedSSPInLast12Weeks";
  public static final String DISPLAY_SSP_END_DATE = "displaySspEndDate";
  public static final String SSP_END_DATE = "sspEndDate";
  public static final String DISPLAY_SSP_RECENT_END_DATE = "displaySspRecentEndDate";
  public static final String SSP_RECENT_END_DATE = "sspRecentEndDate";
  public static final String DAY_AFTER_SSP_RECENT_END_DATE = "dayAfterSspRecentEndDate";
  public static final String STATUTORY_PAY_OTHER = "statutoryPayOther";
  public static final String BACK_TO_WORK_DATE = "backToWorkDate";
  public static final String DISPLAY_BACK_TO_WORK_DATE = "displayBackToWorkDate";
  public static final String CLAIM_END_DATE = "claimEndDate";
  public static final String DISPLAY_CLAIM_END_DATE = "displayClaimEndDate";
  public static final String GETTING_MONEY_FROM_PENSION = "gettingMoneyFromPension";
  public static final String PENSION_INHERIT = "pensionInherit";
  public static final String DISPLAY_PENSION_INHERIT = "displayPensionInherit";
  public static final String PENSION_DETAIL = "pensionDetail";
  public static final String GETTING_MONEY_FROM_INSURANCE = "gettingMoneyFromInsurance";
  public static final String INSURANCE_DETAIL = "insuranceDetail";
  public static final String NINO = "nino";
  public static final String CLAIM_START_DATE = "claimStartDate";
  public static final String SUBMISSION_DATE = "submissionDate";
  public static final String CONDITIONS = "conditions";
  public static final String FIRST_NAME = "firstName";
  public static final String SURNAME = "surName";
  public static final String DOB = "dateOfBirth";
  public static final String HOME_ADDRESS = "homeAddress";
  public static final String HOME_POSTCODE = "homePostcode";
  public static final String DISPLAY_CORRESPONDENCE_ADDRESS = "displayCorrespondenceAddress";
  public static final String CORRESPONDENCE_ADDRESS = "correspondenceAddress";
  public static final String CORRESPONDENCE_POSTCODE = "correspondencePostcode";
  public static final String TELE_MOBILE = "telmobile";
  public static final String TELE_OTHER = "tel";
  public static final String MOBILE_NUMBER = "mobileNumber";
  public static final String OTHER_NUMBER = "otherNumber";
  public static final String DISPLAY_OTHER_NUMBER = "displayOtherNumber";
  public static final String BANK_NAME = "bankName";
  public static final String ACCOUNT_NAME = "accountName";
  public static final String ROLL_NUMBER = "rollNumber";
  public static final String DISPLAY_ROLL_NUMBER = "displayRollNumber";
  public static final String BANK_ACCOUNT = "bankAccount";
  public static final String SORT_CODE = "sortCode";
  public static final String WORKED_ABROAD = "workedAbroad";
  public static final String LIVED_ABROAD = "livedAbroad";
  public static final String UNIVERSAL_CREDIT = "universalCredit";
  public static final String DISPLAY_UNIVERSAL_CREDIT = "displayUniversalCredit";
  public static final String CLAIM_START_DATE_AFTER_SSP = "claimStartDateAfterSsp";
  public static final String DISPLAY_CLAIM_START_DATE_AFTER_SSP = "displayClaimStartDateAfterSsp";

  public static final String DISPLAY_CORONAVIRUS = "displayCoronaVirus";
  public static final String CORONAVIRUS = "coronaVirus";
  public static final String DISPLAY_CORONAVIRUS_REASON = "displayCoronaVirusReason";
  public static final String CORONAVIRUS_REASON = "coronaVirusReason";
  public static final String OTHER_CORONAVIRUS_REASON = "other";
  public static final String DISPLAY_CORONAVIRUS_SHIELD = "displayCoronaVirusShield";
  public static final String CORONAVIRUS_SHIELD = "coronaVirusShield";
  public static final String DISPLAY_CORONAVIRUS_REASON_OTHER = "displayCoronaVirusReasonOther";
  public static final String CORONAVIRUS_REASON_OTHER = "coronaVirusReasonOther";
  public static final String DISPLAY_CORONAVIRUS_DATE = "displayCoronaVirusDate";
  public static final String CORONAVIRUS_REASON_OTHER_DESC = "Other reason";
  public static final String CORONAVIRUS_DATE = "coronaVirusDate";
  public static final String DISPLAY_OTHER_CONDITION = "displayOtherCondition";
  public static final String OTHER_CONDITION = "otherCondition";
  public static final String DISPLAY_CCONDITIONS = "displayConditions";
  public static final String PREGNANCY_DETAIL = "pregnancyDetail";
  public static final String PREGNANCY_DETAIL_DISPLAY = "Yes<br/>\nBaby due on ";
  public static final String DOCTOR_DETAIL = "doctorDetail";
  public static final String LIFE_EXPECTANCY = "lifeExpectancy";
  public static final String SENT_DS1500 = "sentDS1500";
  public static final String DISPLAY_SENT_DS1500 = "displaySentDS1500";
  public static final String LIFE_EXPECTANCY_LESS = "Less than 6 months";
  public static final String LIFE_EXPECTANCY_MORE = "More than 6 months";
  public static final String CURRENTLY_IN_HOSPITAL = "currentlyInHospital";
  public static final String HOSPITAL_DETAIL = "hospitalDetail";
  public static final String DISPLAY_HOSPITAL_DETAIL = "displayHospitalDetail";
  public static final String CAN_SHARE_DETAIL_WITH_DWP = "canShareDetailWithDWP";
  public static final String YES_SHARE_DETAIL_WITH_DWP =
      "Yes, the doctor can share details with DWP";
  public static final String NO_SHARE_DETAIL_WITH_DWP =
      "No, the doctor cannot share details with DWP";
  public static final String CAN_SHARE_DETAIL_WITH_DOC = "canShareDetailWithDoc";
  public static final String YES_SHARE_DETAIL_WITH_DOC =
      "Yes, DWP can share the outcome of an assessment with the doctor";
  public static final String NO_SHARE_DETAIL_WITH_DOC =
      "No, DWP cannot share the outcome of an assessment with the doctor";
  public static final String ADMITTED = "Admitted on ";
  public static final String STARTED = "Started: ";
  public static final String TOTAL_PAID_WORK_COUNT = "totalPaidWorkCount";
  public static final String CURRENT_PAID_WORK_COUNT = "currentPaidWorkCount";
  public static final String JOB_AT = "jobAt";
  public static final String JOB_TITLE = "jobTitle";
  public static final String EMPLOYER_ADDRESS = "employerAddress";
  public static final String EMPLOYER_POSTCODE = "employerPostcode";
  public static final String EMPLOYER_TELEPHONE = "employerTelephone";
  public static final String OFF_SICK_FROM_THIS_WORK = "offSickFromThisWork";
  public static final String DISPLAY_LAST_WORKED = "displayLastWorked";
  public static final String LAST_WORKED = "lastWorked";
  public static final String EMPLOYMENT_STATUS = "employmentStatus";
  public static final String DISPLAY_SUPPORT = "displaySupport";
  public static final String SUPPORT = "support";
  public static final String DISPLAY_EXPENSES_DETAILS = "displayExpensesDetails";
  public static final String EXPENSES_DETAILS = "expensesDetails";
  public static final String DISPLAY_HOURS_WORKED_AT_THIS_JOB = "displayHoursWorkedAtThisJob";
  public static final String HOURS_WORKED_AT_THIS_JOB = "hoursWorkedAtThisJob";
  public static final String DISPLAY_JOB_PAYMENT_AMOUNT = "displayJobPaymentAmount";
  public static final String JOB_PAYMENT_AMOUNT = "jobPaymentAmount";
  public static final String JOB_PAYMENT_FREQUENCY = "jobPaymentFrequency";
  public static final String TOTAL_VOLUNTARY_WORK_COUNT = "totalVoluntaryWorkCount";
  public static final String CURRENT_VOLUNTARY_WORK_COUNT = "currentVoluntaryWorkCount";
  public static final String VOLUNTARY_WORK_AT = "voluntaryWorkAt";
  public static final String VOLUNTARY_WORK_ORG_ADDRESS = "voluntaryWorkOrgAddress";
  public static final String VOLUNTARY_WORK_ORG_POSTCODE = "voluntaryWorkOrgPostcode";
  public static final String ROLE = "role";
  public static final String HOURS_WORKED_AT = "hoursWorkedAt";
  public static final String DIFFERENT_HOURS_WORKED = "Different number of hours each week";
  public static final String TOTAL_PENSION_COUNT = "totalPensionCount";
  public static final String CURRENT_PENSION_COUNT = "currentPensionCount";
  public static final String PENSION_PROVIDER = "pensionProvider";
  public static final String PENSION_DATE_STARTED = "pensionDateStarted";
  public static final String PENSION_PROVIDER_REF = "pensionProviderRef";
  public static final String PENSION_PROVIDER_ADDRESS = "pensionProviderAddress";
  public static final String PENSION_PROVIDER_POSTCODE = "pensionProviderPostcode";
  public static final String PENSION_PROVIDER_TELEPHONE = "pensionProviderTelephone";
  public static final String PAYMENTS_FROM_PENSION = "paymentsFromPension";
  public static final String PENSION_PAYMENTS_FREQUENCY = "pensionPaymentsFrequency";
  public static final String DEDUCTIONS_FROM_PENSION_ANNUITY = "deductionsFromPensionAnnuity";
  public static final String DISPLAY_PENSION_DEDUCTION_DETAILS_YES =
      "displayPensionDeductionDetailsWhenYes";
  public static final String DISPLAY_PENSION_DEDUCTION_DETAILS_NO =
      "displayPensionDeductionDetailsWhenNo";
  public static final String PAYMENTS_FROM_PENSION_BEFORE_DEDUCTION =
      "paymentsFromPensionBeforeDeduction";
  public static final String PAYMENTS_FROM_PENSION_AFTER_DEDUCTION =
      "paymentsFromPensionAfterDeduction";
  public static final String PENSION_DEDUCTION_DETAIL = "pensionDeductionDetail";
  public static final String INHERITED_PENSION = "inheritedPension";
  public static final String INSURANCE_PREMIUM_PAID = "insurancePremiumPaid";
  public static final String INSURANCE_PREMIUM_PAID_MORE =
      "I paid for more than half of the premiums";
  public static final String INSURANCE_PREMIUM_PAID_LESS =
      "I paid for less than half of the premiums";
  public static final String INSURANCE_PROVIDER = "insuranceProvider";
  public static final String TOTAL_INSURANCE_COUNT = "totalInsuranceCount";
  public static final String CURRENT_INSURANCE_COUNT = "currentInsuranceCount";
  public static final String INSURANCE_PROVIDER_REF = "insuranceProviderRef";
  public static final String INSURANCE_PROVIDER_ADDRESS = "insuranceProviderAddress";
  public static final String INSURANCE_PROVIDER_POSTCODE = "insuranceProviderPostcode";
  public static final String INSURANCE_PROVIDER_TELEPHONE = "insuranceProviderTelephone";
  public static final String INSURANCE_REFERENCE_NUMBER = "insuranceReferenceNumber";
  public static final String INSURANCE_STILL_WORK = "stillWorkingForInsuranceCompany";
  public static final String INVALID_DATE = "-0-0";
  public static final String EMPLOYMENT_ENDED_ON = "employmentEndedOn";
  public static final String DISPLAY_EMPLOYMENT_ENDED_ON = "displayEmploymentEndedOn";
  public static final String DATE_NOT_PROVIDED = "Not provided";
  public static final String PAYMENTS_FROM_INSURANCE = "paymentsFromInsurance";
  public static final String INSURANCE_PAYMENTS_FREQUENCY = "insurancePaymentsFrequency";
  public static final String EMP_TYPE_EMPLOYEE = "Employee";
  public static final String EMP_TYPE_SELF_EMPLOYED = "Self-employed";
  public static final String EMP_TYPE_SUB_CONTRACTOR = "Sub-contractor";
  public static final String EMP_TYPE_COMPANY_DIRECTOR = "Company director";
  public static final String SPACE = " ";
  public static final String POUND_SIGN = "&#163;";

  public static final String EXPECTED_DATE_FORMAT = "dd/MM/yy";
  public static final String INPUT_DATE_FORMAT = "yyyy-MM-dd";
  public static final String ALTERNATE_INPUT_DATE_FORMAT = "yyyy-MMM-dd";

}
