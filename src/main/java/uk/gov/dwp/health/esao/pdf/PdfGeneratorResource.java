package uk.gov.dwp.health.esao.pdf;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.text.StringSubstitutor;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.gov.dwp.health.esao.pdf.application.PdfGeneratorConfiguration;
import uk.gov.dwp.health.esao.pdf.application.handlers.HtmlToPdfaHandler;
import uk.gov.dwp.health.esao.pdf.application.util.CustomStringOverride;
import uk.gov.dwp.health.esao.pdf.application.util.DataTransformation;
import uk.gov.dwp.health.esao.pdf.constants.HTMLTemplateFiles;
import uk.gov.dwp.health.esao.pdf.model.RequestJson;
import uk.gov.dwp.health.utilities.JsonObjectSchemaValidation;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.io.IOException;


@Path("/")
public class PdfGeneratorResource {
  private static final Logger LOG = LoggerFactory.getLogger(PdfGeneratorResource.class.getName());
  private static final String ERROR_MSG = "Unable to process request";
  private static final String STANDARD_JSON_ERROR = "Payload contains invalid items";
  private final JsonObjectSchemaValidation jsonObjectSchemaValidation;
  private final PdfGeneratorConfiguration configuration;
  private final HtmlToPdfaHandler htmlToPdfaHandler;
  private final DataTransformation dataTransformation;

  public PdfGeneratorResource(PdfGeneratorConfiguration configuration,
                              JsonObjectSchemaValidation jsonValidator, HtmlToPdfaHandler handler,
                              DataTransformation dataTransformation) {
    this.jsonObjectSchemaValidation = jsonValidator;
    this.configuration = configuration;
    this.htmlToPdfaHandler = handler;
    this.dataTransformation = dataTransformation;
  }

  @POST
  @Path("generatePdf")
  public Response generatePdf(String json) {
    Response response;

    try {
      LOG.info("incoming json '{}'", json);
      // schema validate & serialise json to class
      ObjectMapper objectMapper = new ObjectMapper();

      jsonObjectSchemaValidation.validateJsonDocumentWithFile(json, configuration.getSchemaEntry(),
          configuration.getSchemaValidationFile());

      LOG.info("Validated");
      SimpleModule mod = new SimpleModule();
      mod.addDeserializer(String.class, new CustomStringOverride());
      objectMapper.registerModule(mod);
      String templateHtml =
          IOUtils.toString(getClass().getResourceAsStream(HTMLTemplateFiles.ESAO_TEMPLATE));
      LOG.info("template loaded");

      RequestJson requestJson = objectMapper.readValue(json, RequestJson.class);
      LOG.info("Mapped json");

      if (requestJson.isContentValid()) {
        LOG.info("incoming structure successfully validated");
        StringSubstitutor substitutor =
            new StringSubstitutor(dataTransformation.transformData(requestJson));
        String resolvedHtml = substitutor.replace(templateHtml);

        LOG.debug("loaded html template, ready to substitute data");
        String pdf = htmlToPdfaHandler.generateBase64PdfFromHtml(resolvedHtml);
        response = Response.ok().entity(Base64.decodeBase64(pdf)).build();
      } else {
        LOG.debug("unable to parse input json");
        response = Response.status(HttpStatus.SC_BAD_REQUEST).entity(STANDARD_JSON_ERROR).build();
      }
    } catch (IOException e) {
      LOG.debug(e.getClass().getName(), e);
      LOG.error(e.getMessage());

      response = Response.status(HttpStatus.SC_BAD_REQUEST).entity(ERROR_MSG).build();

    } catch (Exception e) {
      LOG.debug(e.getClass().getName(), e);
      LOG.error(e.getMessage());

      response = Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).entity(ERROR_MSG).build();
    }

    return response;
  }

}
