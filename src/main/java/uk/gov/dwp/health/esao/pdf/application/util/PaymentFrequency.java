package uk.gov.dwp.health.esao.pdf.application.util;

public enum PaymentFrequency {
  DAILY("a day"),
  WEEKLY("a week"),
  BIWEEKLY("every 2 weeks"),
  FOURWEEKLY("every 4 weeks"),
  MONTHLY("a month"),
  QUARTERLY("every 3 months"),
  HALFYEARLY("every 6 months"),
  YEARLY("a year"),
  NONE("");

  private String frequency;

  PaymentFrequency(String frequency) {
    this.frequency = frequency;
  }

  public String getFrequency() {
    return frequency;
  }
}
