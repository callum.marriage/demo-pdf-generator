package uk.gov.dwp.health.esao.pdf.info;

import java.util.Properties;

@FunctionalInterface
public interface InfoProvider {

  Properties getInfo();
}
