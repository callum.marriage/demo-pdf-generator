package uk.gov.dwp.health.esao.pdf.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Application {

  @JsonProperty("description")
  private String description;

  public Application(String description) {
    this.description = description;
  }

  public Application(){

  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
