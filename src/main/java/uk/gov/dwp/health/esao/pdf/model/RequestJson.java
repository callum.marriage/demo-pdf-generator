package uk.gov.dwp.health.esao.pdf.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;

public class RequestJson {

  @JsonProperty("applicant")
  private Applicant applicant;

  @JsonProperty("application")
  private Application application;

  @JsonProperty("msg_id")
  private String messageId;

  @JsonProperty("submission_date")
  private String submissionDate;

  @JsonProperty("reference_number")
  private String referenceNumber;

  public RequestJson(Applicant applicant, Application application, String messageId, String submissionDate, String referenceNumber) {
    this.applicant = applicant;
    this.application = application;
    this.messageId = messageId;
    this.submissionDate = submissionDate;
    this.referenceNumber = referenceNumber;
  }

  public RequestJson(){

  }

  @JsonIgnore
  public boolean isContentValid() {
    return applicant != null && application != null;
  }

  public Application getApplication() {
    return application;
  }

  public void setApplication(Application application) {
    this.application = application;
  }

  public Applicant getApplicant() {
    return applicant;
  }

  public String getMessageId() {
    return messageId;
  }

  public void setMessageId(String messageId) {
    this.messageId = messageId;
  }

  public String getReferenceNumber() {
    return referenceNumber;
  }

  public void setReferenceNumber(String referenceNumber) {
    this.referenceNumber = referenceNumber;
  }

  public String getSubmissionDate() {
    return submissionDate;
  }

  public void setSubmissionDate(String submissionDate) {
    this.submissionDate = submissionDate;
  }
}
