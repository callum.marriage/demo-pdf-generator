package uk.gov.dwp.health.esao.pdf.application.util;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.gov.dwp.health.esao.pdf.model.Applicant;
import uk.gov.dwp.health.esao.pdf.model.Application;
import uk.gov.dwp.health.esao.shared.models.Address;
import uk.gov.dwp.health.esao.shared.models.Conditions;
import uk.gov.dwp.health.esao.shared.models.ContactOptions;
import uk.gov.dwp.health.esao.shared.models.DataCapture;
import uk.gov.dwp.health.esao.shared.models.DeductionDetails;
import uk.gov.dwp.health.esao.shared.models.Employments;
import uk.gov.dwp.health.esao.shared.models.Insurances;
import uk.gov.dwp.health.esao.shared.models.Pensions;
import uk.gov.dwp.health.esao.shared.models.RequestJson;
import uk.gov.dwp.health.esao.shared.models.VoluntaryWork;
import uk.gov.dwp.health.esao.shared.util.StatutoryExtraPaymentEnum;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static uk.gov.dwp.health.esao.pdf.constants.HTMLConstants.*;
import static uk.gov.dwp.health.esao.pdf.constants.HTMLTemplateFiles.*;

@SuppressWarnings("deprecation")
public class DataTransformation {
  private static final Logger LOG = LoggerFactory.getLogger(DataTransformation.class.getName());

  public Map<String, String> transformData(uk.gov.dwp.health.esao.pdf.model.RequestJson requestJson){
    Map<String, String> valuesMap = new HashMap<>();
    Application application = requestJson.getApplication();
    Applicant applicant = requestJson.getApplicant();

    valuesMap.put("referenceNumber",requestJson.getReferenceNumber());

    if( requestJson.getSubmissionDate() != null){
      valuesMap.put("submissionDate", requestJson.getSubmissionDate());
    }
    if(applicant != null){
      valuesMap.put("email", applicant.getEmail());
      valuesMap.put("firstName", applicant.getFirstName());
      valuesMap.put("lastName", applicant.getLastName());

    }

    if(application != null){
      valuesMap.put("description", application.getDescription());
    }

    return valuesMap;
  }
}
