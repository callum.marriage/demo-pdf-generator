package uk.gov.dwp.health.esao.pdf.constants;

public class HTMLTemplateFiles {
  private HTMLTemplateFiles() {
  }

  public static final String ESAO_TEMPLATE = "/ESAO-Template.html";
  public static final String EMPLOYMENTS_TEMPLATE = "/ESAO-Employments-Template.html";
  public static final String VOLUNTARY_WORK_TEMPLATE = "/ESAO-VoluntaryWork-Template.html";
  public static final String PENSION_TEMPLATE = "/ESAO-Pension-Template.html";
  public static final String INSURANCE_TEMPLATE = "/ESAO-Insurance-Template.html";

}
