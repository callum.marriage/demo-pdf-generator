package uk.gov.dwp.health.esao.pdf.application;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import uk.gov.dwp.crypto.SecureStrings;

import javax.crypto.SealedObject;
import javax.validation.constraints.NotNull;
import java.io.File;

public class PdfGeneratorConfiguration extends Configuration {
  private SecureStrings cipher = new SecureStrings();

  @JsonProperty("htmlToPdfServiceUrl")
  private String htmlToPdfServiceUrl;

  @JsonProperty("htmlToPdfConformanceLevel")
  private String htmlToPdfConformanceLevel;

  @JsonProperty("htmlToPdfTruststoreFile")
  private String htmlToPdfTruststoreFile;

  @JsonProperty("htmlToPdfTruststorePass")
  private SealedObject htmlToPdfTruststorePass;

  @JsonProperty("htmlToPdfKeystoreFile")
  private String htmlToPdfKeystoreFile;

  @JsonProperty("htmlToPdfKeystorePass")
  private SealedObject htmlToPdfKeystorePass;

  @NotNull
  @JsonProperty("schemaValidationFile")
  private File schemaValidationFile;

  @NotNull
  @JsonProperty("schemaEntry")
  private String schemaEntry;

  @JsonProperty("applicationInfoEnabled")
  private boolean applicationInfoEnabled;

  public String getHtmlToPdfServiceUrl() {
    return htmlToPdfServiceUrl;
  }

  public String getHtmlToPdfTruststoreFile() {
    return htmlToPdfTruststoreFile;
  }

  public String getHtmlToPdfKeystoreFile() {
    return htmlToPdfKeystoreFile;
  }

  public String getHtmlToPdfTruststorePass() {
    return cipher.revealString(htmlToPdfTruststorePass);
  }

  public void setHtmlToPdfTruststorePass(String htmlToPdfTruststorePass) {
    this.htmlToPdfTruststorePass = cipher.sealString(htmlToPdfTruststorePass);
  }

  public String getHtmlToPdfKeystorePass() {
    return cipher.revealString(htmlToPdfKeystorePass);
  }

  public void setHtmlToPdfKeystorePass(String htmlToPdfKeystorePass) {
    this.htmlToPdfKeystorePass = cipher.sealString(htmlToPdfKeystorePass);
  }

  public String getHtmlToPdfConformanceLevel() {
    return htmlToPdfConformanceLevel;
  }

  public File getSchemaValidationFile() {
    return schemaValidationFile;
  }

  public String getSchemaEntry() {
    return schemaEntry;
  }

  public boolean isApplicationInfoEnabled() {
    return applicationInfoEnabled;
  }
}
