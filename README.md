# ms-pdf-generator [ESA]

This micro-service uses lombok plugin to generate constructors, getters and setters. To enable this feature in intellij, install lombok plugin.

RESTful Webservice taking JSON posted information for the ESA form and rendering into a A1A compliant pdf file.

## Endpoints

* `/generatePdf`

Example input JSON

    {
      "msg_id": "esa.submission.new",
      "ref": "3PYRK7",
      "date_submitted": "2019-04-26T11:56:13.644Z",
      "applicant": {
        "forenames": "Aliyah",
        "surname": "Mante",
        "dob": "1981-04-07",
        "residence_address": {
          "lines": [
            "943 Nolan Parkway",
            "",
            ""
          ],
          "premises": "",
          "postcode": "LS1 1DJ"
        },
        "contact_options": [
          {
            "method": "telmobile",
            "data": "(364) 922-0515",
            "preferred": true
          }
        ]
      },
      "data_capture": {
        "language": "en",
        "conditions": [
          {
            "name": "bluetooth card",
            "start_date": "2003-02-01"
          },
          {
            "name": "back-end capacitor",
            "start_date": "2003-02-01"
          },
          {
            "name": "back-end transmitter",
            "start_date": "2003-02-01"
          }
        ],
        "medical_centre": {
          "name": "Nitzsche, Abbott and Parisian",
          "tel": "(181) 729-7881",
          "address": {
            "lines": [
              "707 Stephanie Garden",
              "",
              "Damonhaven"
            ],
            "premises": "",
            "postcode": "LS1 1DJ"
          },
          "doctor": "Dr. Wilfredo Kassulke"
        },
        "statutory_pay_other": "none",
        "nino": "AS123123D",
        "bank_account_name": "Mariam King",
        "bank_name": "Prohaska - Fisher",
        "bank_sort_code": "011934",
        "bank_account_number": "12345678",
        "claim_start_date": "2019-04-05",
        "back_to_work": "no",
        "pregnant": "no",
        "severe_condition": "no",
        "hospital_inpatient": "yes",
        "hospital_name": "Bernhard Inc",
        "hospital_ward": "Gottlieb - Senger",
        "hospital_admission_date": "2019-01-26",
        "doc_share_with_dwp": "yes",
        "dwp_share_with_doc": "yes",
        "work_overseas": "yes",
        "military_overseas": "no",
        "voluntary_work_question": "yes",
        "voluntary_work": [
          {
            "organisation_name": "Hirthe, Lemke and Yundt",
            "organisation_address": {
              "lines": [
                "888 Lindgren Junctions",
                "",
                ""
              ],
              "premises": "",
              "postcode": "LS1 1DJ"
            },
            "role": "Laborum voluptas enim ut. Praesentium et voluptates sunt debitis nisi molestiae soluta. Aliquid voluptas perspiciatis impedit deserunt. Velit perferendis minus velit minima maxime atque. Ut itaque eum nostrum delectus consequuntur sed dolorem magnam.",
            "same_hours": "yes",
            "hours": "1"
          },
          {
            "organisation_name": "Quitzon Inc",
            "organisation_address": {
              "lines": [
                "983 Hyatt Well",
                "",
                ""
              ],
              "premises": "",
              "postcode": "LS1 1DJ"
            },
            "role": "Architecto et molestiae. Non at vel eligendi sunt nemo qui qui. Consequatur quo consectetur necessitatibus et sunt rerum perspiciatis nobis quod. Molestiae quam natus esse in repellendus tenetur dolorem officia. Sint nihil provident culpa qui qui quia aut fugiat voluptates.",
            "same_hours": "yes",
            "hours": "1"
          },
          {
            "organisation_name": "Schaden Group",
            "organisation_address": {
              "lines": [
                "395 Zemlak Rapid",
                "",
                ""
              ],
              "premises": "",
              "postcode": "LS1 1DJ"
            },
            "role": "Ut et libero animi laborum et enim esse sit. Et rerum est ex deleniti recusandae soluta. Nihil non adipisci quia error. Consequatur eligendi nisi. Exercitationem id perspiciatis quasi consequatur praesentium.",
            "same_hours": "yes",
            "hours": "1"
          }
        ],
        "employment_question": "yes",
        "employments": [
          {
            "job_title": "Senior Data Engineer",
            "employer_name": "Bernier, Konopelski and DAmore",
            "employer_tel": "(966) 744-4421",
            "employer_address": {
              "lines": [
                "767 Franecki Turnpike",
                "",
                "South Brookhaven"
              ],
              "premises": "",
              "postcode": "LS1 1DJ"
            },
            "employment_status": [
              "employee",
              "subContractor"
            ],
            "off_sick": "no",
            "same_hours": "yes",
            "hours": "12",
            "net_pay": "1234",
            "frequency": "every4Weeks",
            "support": "yes",
            "expenses_question": "yes",
            "expenses_details": "Et id quia repellat exercitationem quis beatae quo omnis sed. Animi voluptatem non maiores perspiciatis. Consequuntur consequatur reprehenderit consequatur consequatur voluptas ea quis libero."
          }
        ],
        "ssp": "yes",
        "ssp_end": "2019-05-17",
    "pension_question" : "yes",
           "pensions" : [
               {
                   "pension_provider" : "",
                   "provider_ref" : "hjkh",
                   "provider_tel" : "",
                   "provider_address" : {
                       "lines" : [
                           "",
                           "",
                           ""
                       ],
                       "premises" : "",
                       "postcode" : ""
                   },
                   "start_date" : "",
                   "deductions" : "yes",
                   "amount_gross" : "",
                   "frequency" : "weekly",
                   "amount_net" : "",
                   "inherited" : "",
                   "deduction_details" : [
                       {
                           "amount" : "",
                           "detail" : ""
                       }
                   ]
               }
           ],
           "insurance_question" : "yes",
           "insurance" : [
               {
                   "insurance_provider" : "",
                   "provider_ref" : "jhjkh",
                   "provider_tel" : "",
                   "provider_address" : {
                       "lines" : [
                           "",
                           "",
                           ""
                       ],
                       "premises" : "",
                       "postcode" : ""
                   },
                   "amount" : "",
                   "premiums" : "notsure"
               }
           ],
        "mobile": "yes"
      },
      "declaration": "I declare",
      "tags": []
    }

Outputs

* Invalid json input data - `BAD_REQUEST (400)`
* Exceptions during processing - `INTERNAL_SERVER_ERROR (500)`
* Success = encoded string of the resultant pdf file byte array - `SC_OK (200) with payload`

### Continuous Integration (CI) Pipeline

For general information about the CI pipeline on this repository please see documentation at: https://confluence.service.dwpcloud.uk/x/_65dCg

**Pipeline Invocation**

This CI Pipeline now replaces the Jenkins Build CI Process for the `ms-pdf-generator`.

Gitlab CI will automatically invoke a pipeline run when pushing to a feature branch (this can be prevented using `[skip ci]` in your commit message if not required).

When a feature branch is merged into `develop` it will automatically start a `develop` pipeline and build the required artifacts.

For production releases please see the release process documented at: https://confluence.service.dwpcloud.uk/pages/viewpage.action?spaceKey=DHWA&title=SRE
A production release requires a manual pipeline (to be invoked by an SRE) this is only a release function. 
Production credentials are required.

**localdev Usage**

There is no change to the usage of localdev. The gitlab CI Build process create artifacts using the same naming convention as the old (no longer utilised) Jenkins CI Build process.

Therefore please continue to use `branch-develop` or `branch-f-*` (depending on branch name) for proving any feature changes.

**Access**

While this repository is open internally for read, no one has write access to this repository by default.
To obtain access to this repository please contact #ask-health-platform within slack and a member will grant the appropriate level of access.
